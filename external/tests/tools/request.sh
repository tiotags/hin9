
#set -o xtrace

if [ -n "$LOCAL_PATH" ]; then
  md5_orig=`md5sum "$LOCAL_PATH" | awk '{ print $1 }'`
elif [ -n "$PHP_PATH" ]; then
  md5_orig=`php "$PHP_PATH" | md5sum | awk '{ print $1 }'`
elif [ -n "$LOCAL_DATA" ]; then
  md5_orig=`printf "$LOCAL_DATA" | md5sum | awk '{ print $1 }'`
else
  echo "no file specified"
fi

check_output () {
  if [ $RET_CODE -ne 0 ]; then
    echo "command failed"
    exit 1
  fi

  md5_new=`md5sum "$out_file" | awk '{ print $1 }'`
  printf "$LOCAL_PATH\norig: $md5_orig\n"
  echo "$URL_PATH"
  echo "new:  $md5_new"

  if [ "$md5_orig" != "$md5_new" ]; then
    echo "$name $module doesn't match"
    exit 1
  fi
  echo "Completed test $name $module"
}

check_hammer () {
  export RET
  export module
  if sh $TOOL_DIR/hammer.sh; then
    echo "Hammer ok!"
  else
    echo "Hammer failed!"
    exit 1
  fi
}

export module="normal"
if echo "$SUBTEST" | grep "$module" > /dev/null; then
  echo "Start test $name $module"
  out_file=$BENCH_BINARY_DIR/${name}_${module}.bin
  curl $BENCH_CURL_FLAGS -o "$out_file" "http://$BENCH_HOST:$BENCH_PORT/$URL_PATH"
  RET_CODE=$?
  check_output
fi

export module="ssl"
if echo "$SUBTEST" | grep "$module" > /dev/null; then
  echo "Start test $name $module"
  out_file=$BENCH_BINARY_DIR/${name}_${module}.bin
  curl $BENCH_CURL_FLAGS -o "$out_file" "https://$BENCH_HOST:$BENCH_PORTS/$URL_PATH"
  RET_CODE=$?
  check_output
fi

export module="deflate"
if echo "$SUBTEST" | grep "$module" > /dev/null; then
  echo "Start test $name $module"
  out_gz=${BENCH_BINARY_DIR}/${name}_${module}.gz
  out_file=${BENCH_BINARY_DIR}/${name}_${module}.bin
  RET="$(curl $BENCH_CURL_FLAGS -o "$out_gz" --header "Accept-Encoding: deflate" "http://$BENCH_HOST:$BENCH_PORT/$URL_PATH")"
  RET_CODE=$?
  # TODO
  if echo "$RET" | grep "deflate" > /dev/null; then
    cat $out_gz | zlib-flate -uncompress > $out_file
    check_output
  fi
fi

export module="post"
if echo "$SUBTEST" | grep "$module" > /dev/null; then
  echo "Start test $name $module"
  POSTVALUE="helloworldhowareyou"
  out_file=$BENCH_BINARY_DIR/${name}_${module}.bin
  curl $BENCH_CURL_FLAGS --form VALUE="$POSTVALUE" -o "$out_file" "http://$BENCH_HOST:$BENCH_PORT/$URL_PATH"
  RET_CODE=$?
  A1=`grep $POSTVALUE $out_file`
  A2="  <tr><td>VALUE</td><td>$POSTVALUE</td></tr>"
  if [ "$A1" != "$A2" ]; then
    echo "Output doesn't match"
    exit 1
  fi
  echo "Completed test $name $module"
fi

check_range () {
  md5_backup="$md5_orig"
  md5_orig=`dd bs=1MB skip=$RANGE_START count=$RANGE_COUNT if="$LOCAL_PATH"  status=none iflag=skip_bytes,count_bytes oflag=seek_bytes | md5sum | awk '{ print $1 }'`
  out_file=$BENCH_BINARY_DIR/${name}_${module}.bin
  curl $BENCH_CURL_FLAGS -r $RANGE_REQUEST -o "$out_file" "http://$BENCH_HOST:$BENCH_PORT/$URL_PATH"
  RET_CODE=$?
  check_output
  md5_orig="$md5_backup"
}

export module="range"
if echo "$SUBTEST" | grep "$module" > /dev/null; then
  echo "Start test $name $module"
  FILE_SIZE=`stat -c %s "$LOCAL_PATH"`
  BLOCK_SIZE=5000

  RANGE_START=5000
  RANGE_COUNT=$BLOCK_SIZE
  RANGE_REQUEST="$RANGE_START-$((RANGE_START + BLOCK_SIZE - 1))"
  check_range

  RANGE_START=$((FILE_SIZE - BLOCK_SIZE))
  RANGE_COUNT=$((FILE_SIZE - RANGE_START))
  RANGE_REQUEST="$RANGE_START-"
  check_range

  RANGE_START=$((FILE_SIZE - BLOCK_SIZE))
  RANGE_COUNT=$BLOCK_SIZE
  RANGE_REQUEST="-$BLOCK_SIZE"
  check_range
fi

export module="no_keepalive"
if echo "$SUBTEST" | grep "$module" > /dev/null; then
  echo ""
  echo "Start test $name $module"
  RET="$(ab -c $BENCH_CON -n $BENCH_NUM "http://$BENCH_HOST:$BENCH_PORT/$URL_PATH" 2>&1)"
  RET_CODE=$?
  export RET_CODE
  check_hammer
  echo "Completed test $name $module"
fi

export module="hammer"
if echo "$SUBTEST" | grep "$module" > /dev/null; then
  echo ""
  echo "Start test $name $module"
  RET="$(ab -k -c $BENCH_CON -n $BENCH_NUM "http://$BENCH_HOST:$BENCH_PORT/$URL_PATH"  2>&1)"
  RET_CODE=$?
  export RET_CODE
  check_hammer
  echo "Completed test $name $module"
fi

export module="head"
if echo "$SUBTEST" | grep "$module" > /dev/null; then
  echo ""
  echo "Start test $name $module"
  RET="$(ab -k -i -c $BENCH_CON -n $BENCH_NUM "http://$BENCH_HOST:$BENCH_PORT/$URL_PATH" 2>&1)"
  RET_CODE=$?
  export RET_CODE
  if [[ ! $( echo "$RET" | grep "HTML transferred:       0 bytes" ) ]]; then
    echo "$RET"
    echo "Head requests sent bytes when it shouldn't"
    exit 1
  fi
  check_hammer
  echo "Completed test $name $module"
fi


