
set -e

tmp_path="$BENCH_BINARY_DIR/temp.bin"
out_dir="$BENCH_HTDOCS_DIR/$BENCH_HTDOCS_TEST_DIR"
medium_path="$out_dir/$TEST_FILE_MEDIUM"
large_path="$out_dir/$TEST_FILE_LARGE"
text_path="$out_dir/$TEST_FILE_TEXT"
forbidden_path="$out_dir/forbidden.html"

mkdir -p "$out_dir"

if [ ! -f "$tmp_path" ]; then
  echo "generating $tmp_path"
  dd if=/dev/urandom of=$tmp_path bs=1M count=5
fi

if [ ! -f "$medium_path" ]; then
  echo "generating $medium_path"
  for i in {1..3}
  do
    cat $tmp_path >> $medium_path
  done
fi

if [ ! -f "$large_path" ]; then
  echo "generating $large_path"
  for i in {1..100}
  do
    cat $tmp_path >> $large_path
  done
fi

if [ ! -f "$text_path" ]; then
  echo "generating $text_path"
  curl -o $text_path https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js
fi

if [ ! -f "$forbidden_path" ]; then
  echo "generating $forbidden_path"
  echo "Hello world" > $forbidden_path
  chmod -rwx $forbidden_path
  #chown nobody:nobody $forbidden_path
fi


