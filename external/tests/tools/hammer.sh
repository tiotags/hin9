
echo "$name $module hammer parse"

echo "$RET"

set +e

if [ $RET_CODE -ne 0 ]; then
  echo "command failed"
  exit 1
fi

failed_req=`echo "$RET" | grep "Failed requests"`

total=`echo "$RET" | grep "Requests per second"`
if [ -z "$NO_BENCH" ]; then
  if echo "$failed_req" | grep "Failed requests:        0"
  then
    failed_req=""
  fi
  printf "${name} ${module} $total \t$failed_req\n" >> $BENCH_RESULTS_DIR/bench.txt
fi

sock_recv=`echo "$RET" | grep "apr_socket_recv"`
if [ -n "$sock_recv" ]; then
  echo "$name $module got socket errors requests $sock_recv"
  exit 1
fi

non200=`echo "$RET" | grep "Non-2xx responses"`
if [ -n "$non200" ]; then
  echo "$name $module got non 200 requests $non200"
  exit 1
fi
failed=`echo "$RET" | grep "Exceptions:"`
if [ -n "$failed" ]; then
  echo "$name $module got failed requests $failed"
  exit 1
fi
failed=`echo "$RET" | grep "apr_pollset_poll"`
if [ -n "$failed" ]; then
  echo "$name $module got timedout requests $failed"
  exit 1
fi

set -e

echo "$name $module hammer finished"
