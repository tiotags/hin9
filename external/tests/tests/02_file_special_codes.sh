
set +e

check_error () {
  RET_CODE=$?
  echo "$RET"
  echo "ret code is $RET_CODE"
  if [ $RET_CODE -eq 0 ]; then
    echo "command succeded when it shouldn't ?"
    exit 1
  fi
  if echo "$RET" | grep "returned error: $ERROR_CODE" > /dev/null; then
    echo "found proper return code $ERROR_CODE"
  else
    echo "command didn't find the required error code $ERROR_CODE"
    exit 1
  fi
}

ERROR_CODE=404
RET="$(curl --fail "http://$BENCH_HOST:$BENCH_PORT/$BENCH_HTDOCS_TEST_DIR/404" 2>&1)"
check_error

ERROR_CODE=403
RET="$(curl --fail "http://$BENCH_HOST:$BENCH_PORT/$BENCH_HTDOCS_TEST_DIR/forbidden.html" 2>&1)"
check_error

echo "test finished successfully"
