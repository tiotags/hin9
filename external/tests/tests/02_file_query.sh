
set -e

#query
export URL_PATH=index.html?hello
export LOCAL_PATH=$BENCH_HTDOCS_DIR/index.html
export SUBTEST="normal ssl deflate gzip"

sh $TOOL_DIR/request.sh

# path_info
export URL_PATH=index.html/hello
export LOCAL_PATH=$BENCH_HTDOCS_DIR/index.html
export SUBTEST="normal ssl deflate gzip"

sh $TOOL_DIR/request.sh

