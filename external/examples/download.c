
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "http/http.h"
#include "conf.h"

// used to track errors and finished state
static int state_callback (http_client_t * http, uint32_t state, uintptr_t data) {
  switch (state) {
  case HIN_HTTP_STATE_SSL_FAILED: // fall-through
  case HIN_HTTP_STATE_CONNECTION_FAILED:
  case HIN_HTTP_STATE_HEADERS_FAILED:
  case HIN_HTTP_STATE_ERROR:
  case HIN_HTTP_STATE_FINISH:
    fprintf (stderr, "done.\n");
    hin_stop ();
  break;
  default:
  break;
  }
  return 0;
}

http_client_t * http_download_raw (const char * url1) {
  // get a connection
  http_client_t * http = http_connection_get (url1);
  if (http == NULL) {
    state_callback (http, HIN_HTTP_STATE_CONNECTION_FAILED, 0);
    return NULL;
  }

  http->debug = hin_g.debug;

  http->state_callback = state_callback;

  http_connection_start (http);

  return http;
}

/*
  download an url to std out
*/
int main (int argc, const char * argv[]) {
  // initialize basic data
  memset (&hin_g, 0, sizeof hin_g);
  hin_g.debug = 0;

  hin_init ();

  // start a download object
  http_download_raw ("https://tiotags.gitlab.io/");

  // process all events and exit when hin_stop is issued
  hin_event_loop ();

  hin_clean ();
  return 0;
}



