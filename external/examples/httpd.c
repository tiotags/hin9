
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hin.h"
#include "http/http.h"
#include "http/vhost.h"

int hin_server_callback (httpd_client_t * http) {
  httpd_respond_text (http, 200, "Hello\n");
  return 0;
}

httpd_server_t * httpd_create (const char * addr, const char * port, const char * sock_type, void * ssl_ctx) {
  httpd_server_t * server = calloc (1, sizeof *server);
  server->process_callback = hin_server_callback;

  if (hin_httpd_start (server, addr, port, sock_type, ssl_ctx) == 0) {
    free (server);
    return NULL;
  }

  return server;
}

/*
  start a http server that only servers a static response
*/
int main (int argc, char * argv[]) {
  memset (&hin_g, 0, sizeof hin_g);
  hin_g.debug = HNDBG_BASIC;

  hin_init ();

  httpd_create (NULL, "8080", NULL, NULL);

  hin_event_loop ();

  hin_clean ();
  return 0;
}


