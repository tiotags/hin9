cmake_minimum_required(VERSION 3.10)

# set the project name
project (hin9_examples)

add_definitions (-D_GNU_SOURCE -D_LARGEFILE_SOURCE -Wall -Werror=vla)

include_directories ("../basic/include/" "../../src/hin")

set (TARGET_LIBS ${TARGET_LIBS} uring z crypto ssl ${CMAKE_CURRENT_BINARY_DIR}/libhin.so)

set (null_httpd_src
httpd/client.c
httpd/httpd.c
httpd/main.c
httpd/read.c
httpd/utils.c
)

add_executable (old_httpd ${null_httpd_src})
target_link_libraries (old_httpd ${TARGET_LIBS})

add_executable (download download.c)
target_link_libraries (download ${TARGET_LIBS})

add_executable (save_to_file save_to_file.c)
target_link_libraries (save_to_file ${TARGET_LIBS})

add_executable (upload upload.c)
target_link_libraries (upload ${TARGET_LIBS})

add_executable (upload_file upload_file.c)
target_link_libraries (upload_file ${TARGET_LIBS})

add_executable (progress progress.c)
target_link_libraries (progress ${TARGET_LIBS})

add_executable (httpd httpd.c)
target_link_libraries (httpd ${TARGET_LIBS})


