
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "http/http.h"
#include "conf.h"

static int hin_post_done_callback (hin_pipe_t * pipe) {
  http_client_t * http = pipe->parent;

  if (pipe->debug & (HNDBG_POST|HNDBG_PIPE))
    printf ("pipe %d>%d post done %lld\n", pipe->in.fd, pipe->out.fd, (long long)pipe->out.count);

  http->io_state &= ~HIN_REQ_POST;

  return 0;
}

// used to track errors and finished state
static int state_callback (http_client_t * http, uint32_t state, uintptr_t data) {
  switch (state) {
  case HIN_HTTP_STATE_SSL_FAILED: // fall-through
  case HIN_HTTP_STATE_CONNECTION_FAILED:
  case HIN_HTTP_STATE_HEADERS_FAILED:
  case HIN_HTTP_STATE_ERROR:
  case HIN_HTTP_STATE_FINISH:
    fprintf (stderr, "done.\n");
    hin_stop ();
  break;
  case HIN_HTTP_STATE_SEND:
  break;
  default:
  break;
  }
  return 0;
}

http_client_t * http_download_upload (const char * url1, const char * post_path) {
  // get a connection and setup basic parameters
  http_client_t * http = http_connection_get (url1);
  http->debug = hin_g.debug;
  http->state_callback = state_callback;

  // open the posted file
  FILE * fp = fopen (post_path, "r");
  if (fp == NULL) {
    printf ("can't open '%s' %s\n", post_path, strerror (errno));
    exit (1);
  }
  fseek (fp, 0L, SEEK_END);
  off_t sz = ftell (fp);

  // create a pipe for the posted file
  hin_pipe_t * pipe = calloc (1, sizeof (*pipe));
  hin_pipe_init (pipe);
  pipe->in.fd = fileno (fp);
  pipe->in.flags = HIN_FILE | HIN_OFFSETS;
  pipe->parent = http;
  pipe->finish_callback = hin_post_done_callback;
  pipe->debug = http->debug;

  pipe->in.flags |= HIN_COUNT;
  pipe->sz = sz;

  // create wrapper around pipe
  hin_http_post_t * post = calloc (1, sizeof *post);
  post->pipe = pipe;
  post->content_type = strdup ("multipart/form-data;boundary=\"boundary\"");

  http->upload = post;

  http_connection_start (http);

  return http;
}

/*
  http POST example, the payload has to be in a valid POST format
*/
int main (int argc, const char * argv[]) {
  // initialize basic data
  memset (&hin_g, 0, sizeof hin_g);
  hin_g.debug = 0;

  hin_init ();

  // parse arguments
  if (argc < 2) {
    printf ("usage %s <url> <post payload>\n", argv[0]);
    exit (1);
  }

  // an example post file is in external/examples/post.txt
  const char * url = argv[1];
  const char * post_path = argv[2];

  // start a download object
  http_download_upload (url, post_path);

  // process all events and exit when hin_stop is issued
  hin_event_loop ();

  hin_clean ();
  return 0;
}



