#!/bin/sh

set -e

USER=docker

apk update && apk upgrade
apk add nano bash sudo openssl curl coreutils xxd rsync
apk add ninja build-base cmake lua-dev linux-headers openssl-dev zlib-dev liburing-dev

# for building abuilds
#apk add abuild
#adduser $USER wheel
#adduser $USER abuild
#echo '%wheel ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
#sudo -u $USER abuild-keygen -a -n -i

DIR=`pwd`

mkdir $DIR/build
cd $DIR/build
cmake -GNinja ..
ninja

cp $DIR/build/hinsightd /usr/local/bin/hinsightd


