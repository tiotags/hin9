#ifndef BASIC_PATTERN_H
#define BASIC_PATTERN_H

#include <stdarg.h>
#include <stdint.h>

// 2 types of strings, interned and buffers

enum {
PATTERN_NIL	= 0,
PATTERN_LETTER	= 0x1,	PATTERN_SPACE	= 0x2,
PATTERN_CONTROL	= 0x4,	PATTERN_DIGIT	= 0x8,
PATTERN_UPPER	= 0x10, PATTERN_PUNCTUATION= 0x20,
PATTERN_HEXA	= 0x40,

PATTERN_CUSTOM=0x100, PATTERN_NULL_CHAR=0x200,
PATTERN_LOWER=0x400,  PATTERN_ALPHANUM=PATTERN_LETTER|PATTERN_DIGIT,
PATTERN_NEGATIVE=0x1000, PATTERN_ALL=0x2000,
};

enum {
PATTERN_CASE = 0x1, PATTERN_INVERSE = 0x2,
PATTERN_INIT = 0x4,
};

typedef struct {
  char * ptr;
  uintptr_t len;
} string_t;

int match_string_virtual (string_t *data, uint32_t flags, const char *format, va_list argptr);

int match_string (string_t *data, const char *format, ...);
int matchi_string (string_t *data, const char *format, ...);

int match_string_equal (string_t * source, const char * format, ...);
int matchi_string_equal (string_t * source, const char * format, ...);

#endif
