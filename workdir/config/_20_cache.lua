
create_cache ("/tmp/cache/", 0)

-- default map
map (nil, "*", const.MAP_PREFILE, function (req)
  local path = req_get_path (req)
  local query = req_get_query (req)
  local vhost = req_get_vhost (req)

  req_set_cache_key (req, vhost, ":", path, "?", query)
end)

