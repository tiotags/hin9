
Major goals
* to create a simple webserver that uses both threading and event based processing for faster processing
* simple to understand and well documented config file
* powerful and flexible config file
* well protected against DoS attacks

Bugs
* only unreported ones for the moment, but I don't test very well

Roadmap
* merge hin10 into hin9 so we can use those beautiful threads
* a simple config file that can run without lua, again a work in progress in the hin10 branch
* more testing and better testing
* better DDoS protection and DoS in general

Sandboxing
* syscall filtering
* namespaces

For the future
* traffic shaping
* refactor cache subsystem
* improve and test reverse proxy code
* wildcard vhosts
* better error handling
* cgi spec compliance
* file based cache for gzip/deflate files


