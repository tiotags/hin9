
requirements
------------

requires bash-like shell, coreutils, curl, apache bench, netcat, perl

for php: requires a php-fpm listening on tcp port 9000

for rproxy: requires another http server listening on port 28081 with the same htdocs as this one and php enabled

you can run all tests by executing `sh external/tests/run.sh`

to run a single test `sh run.sh tests/<name>.sh`

benchmarks results are saved in `build/test/logs/bench.txt`


