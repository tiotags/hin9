
#ifndef HIN_MASTER_H
#define HIN_MASTER_H

enum {
HIN_FLAG_RUN = 0x1, HIN_FLAG_QUIT = 0x2,
HIN_DAEMONIZE = 0x4, HIN_PRETEND = 0x8,
HIN_CREATE_DIRECTORY = 0x10, HIN_SKIP_CONFIG = 0x20,
HIN_VERBOSE_ERRORS = 0x40, HIN_FLAG_FINISH = 0x80,
HIN_FLAG_INIT = 0x100, HIN_FORCE_EPOLL = 0x200,
};

enum {HIN_MASTER_RUN = 0, HIN_MASTER_QUIT, HIN_MASTER_RESTART};

typedef struct {
  uint32_t flags;
  uint32_t debug;
  uint32_t state;
  int num_listen;
  int num_client, max_client;
  int num_connection, num_idle;
  off_t cork_size;
  basic_dlist_t server_list;
  basic_dlist_t server_retry;
  basic_dlist_t connection_idle;
  basic_dlist_t connection_list;
  basic_dlist_t vhost_list;
} hin_master_global_t;

extern hin_master_global_t hin_g;

int hin_init ();
int hin_timer_init (int (*callback) (int ms));
int hin_clean ();
#define hin_event_process() 
int hin_event_wait ();
void hin_event_loop ();

void hin_stop ();
int hin_check_alive ();

#endif

