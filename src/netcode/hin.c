
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "hin_internal.h"

HIN_EXPORT hin_master_global_t hin_g;

HIN_EXPORT int hin_init () {
  if (hin_event_init () < 0) {
    hin_g.flags |= HIN_FORCE_EPOLL;
  }
  hin_timer_init (NULL);
  return 0;
}

HIN_EXPORT int hin_clean () {
  hin_event_clean ();
  hin_epoll_clean ();
  hin_timer_clean ();
  hin_ssl_cleanup ();
  return 0;
}

HIN_EXPORT void hin_stop () {
  if (hin_g.flags & HIN_FLAG_QUIT) return;
  hin_g.flags |= HIN_FLAG_QUIT;
  hin_g.state = HIN_MASTER_QUIT;

  basic_dlist_t * elem = hin_g.server_list.next;
  if (elem == NULL) {
    hin_check_alive ();
  }
  while (elem) {
    hin_server_t * server = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;

    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("stopping server %d\n", server->c.sockfd);
    hin_server_close (server);
  }
}

static void hin_show_alive () {
  basic_dlist_t * elem = hin_g.server_list.next;
  while (elem) {
    hin_server_t * server = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;

    hin_debug (" alive server %d\n", server->c.sockfd);

    basic_dlist_t * elem1 = server->client_list.next;
    while (elem1) {
      hin_client_t * client = basic_dlist_ptr (elem1, offsetof (hin_client_t, list));
      elem1 = elem1->next;

      hin_debug ("  c %d\n", client->sockfd);
    }
  }

  elem = hin_g.connection_list.next;
  while (elem) {
    hin_client_t * c = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;

    hin_debug (" alive client %d\n", c->sockfd);
  }
}

HIN_EXPORT int hin_check_alive () {
  //printf ("check alive %d server %p connect %p\n", hin_g.state, hin_g.server_list.next, hin_g.connection_list.next);
  if (hin_g.state != HIN_MASTER_QUIT) {
    return 1;
  }

  hin_stop ();

  if (hin_g.server_list.next || hin_g.connection_list.next || hin_g.num_connection > 0) {
    if (hin_g.debug & HNDBG_CONFIG) {
      hin_debug ("hin live client %d conn %d\n", hin_g.num_client, hin_g.num_connection);
      hin_show_alive ();
    }

    return 1;
  }
  hin_g.flags |= HIN_FLAG_FINISH;
  return 0;
}




