
#ifndef HIN_SSL_H
#define HIN_SSL_H

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>

#include <basic_lists.h>

enum { HIN_SSL_READ = 0x1, HIN_SSL_WRITE = 0x2 };

typedef struct hin_ssl_struct {
  SSL *ssl;
  BIO *rbio;	// SSL reads from, we write to
  BIO *wbio;	// SSL writes to, we read from

  uint32_t flags;
  basic_dlist_t read_list, write_list;
} hin_ssl_t;

#endif

