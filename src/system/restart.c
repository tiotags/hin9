
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "hin.h"

#include "hin_app.h"
#include "hin_child.h"

static void hin_restart_do_exec (int pipe_fd) {
  printf ("restart to '%s'\n", hin_app.exe_path);

  hin_clean ();

  char * buf = NULL;
  if (asprintf (&buf, "%d", pipe_fd) < 0)
    perror ("asprintf");
  const char ** argv = calloc (20, sizeof (char*));
  int i=0;
  argv[i++] = hin_app.exe_path;
  if (hin_app.conf_path) {
    argv[i++] = "--config";
    argv[i++] = hin_app.conf_path;
  }
  if (hin_app.conf_path) {
    argv[i++] = "--workdir";
    argv[i++] = hin_app.workdir_path;
  }
  if (hin_app.conf_path) {
    argv[i++] = "--logdir";
    argv[i++] = hin_app.logdir_path;
  }
  if (hin_app.conf_path) {
    argv[i++] = "--tmpdir";
    argv[i++] = hin_app.tmpdir_path;
  }
  if (hin_app.pid_path) {
    argv[i++] = "--pidfile";
    argv[i++] = hin_app.pid_path;
  }
  argv[i++] = "--reuse";
  argv[i++] = buf;
  argv[i++] = NULL;
  execvp (hin_app.exe_path, (char * const*)argv);
  hin_error ("execvp '%s': %s", hin_app.exe_path, strerror (errno));
  exit (-1);
}

void hin_app_restart_finished () {
  if (hin_app.restart_pipe_fd) {
    const char * msg = "restart done\n";
    int ret = write (hin_app.restart_pipe_fd, msg, strlen (msg));
    if (ret < 0) hin_perror ("write");
    printf ("restart done\n");
  }
}

static int hin_restart_pipe_read_callback (hin_buffer_t * buf, int ret) {
  if (ret <= 0) {
    if (ret < 0) {
      hin_error ("restart %s", strerror (-ret));
    }
    hin_error ("restart crashed");
    return 1;
  }
  printf ("restart succeeded\n");
  hin_app_stop ();
  return 1;
}

int hin_app_restart () {
  printf("hin restart ...\n");

  hin_pidfile_clean ();

  int info_pipe[2];
  if (pipe (info_pipe) < 0) {
    perror ("pipe");
    return -1;
  }

  int pid = fork ();
  if (pid < 0) {
    perror ("fork");
    return -1;
  }
  if (pid == 0) {
    close (info_pipe[0]);
    hin_restart_do_exec (info_pipe[1]);
    return 0;
  }

  close (info_pipe[1]);

  hin_buffer_t * buf = malloc (sizeof (*buf) + READ_SZ);
  memset (buf, 0, sizeof (*buf));
  #ifdef HIN_LINUX_BUG_5_11_3
  buf->flags = HIN_EPOLL;
  #endif
  buf->fd = info_pipe[0];
  buf->callback = hin_restart_pipe_read_callback;
  buf->count = buf->sz = READ_SZ;
  buf->ptr = buf->buffer;
  buf->debug = hin_g.debug;

  if (hin_request_read (buf) < 0) {
    hin_weird_error (54222111);
    hin_buffer_clean (buf);
    return -1;
  }

  printf ("restarting %d helper %d\n", getpid (), pid);

  return 0;
}

