
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <pwd.h>
#include <grp.h>

#include "hin.h"

int hin_drop_user (const char * username, const char * groupname) {
  int ret = -1, err = 0;
  struct passwd *pwd = calloc (1, sizeof (struct passwd));
  struct group *grp = calloc (1, sizeof (struct group));
  size_t pwd_len = sysconf (_SC_GETPW_R_SIZE_MAX);
  char *pwd_buf = calloc (pwd_len, 1);
  size_t grp_len = sysconf (_SC_GETGR_R_SIZE_MAX);
  char *grp_buf = calloc (grp_len, 1);

  if (pwd == NULL || grp == NULL || pwd_buf == NULL || grp_buf == NULL) {
    hin_error ("drop user no mem");
    goto finish;
  }

  err = getpwnam_r (username, pwd, pwd_buf, pwd_len, &pwd);
  if (pwd == NULL || err) {
    if (err)
      hin_perror ("getpwnam_r");
    hin_error ("failed to find user %s", username);
    goto finish;
  }
  //printf("uid: %d\n", pwd->pw_uid);
  //printf("gid: %d\n", pwd->pw_gid);
  err = getgrnam_r (groupname, grp, grp_buf, grp_len, &grp);
  if (grp == NULL || err) {
    if (err)
      hin_perror ("getgrnam_r");
    hin_error ("failed to find group %s", groupname);
    goto finish;
  }
  //printf("gid: %d\n", grp->gr_gid);
  int userid = pwd->pw_uid;
  int groupid = grp->gr_gid;

  if (chdir ("/") != 0) {
    hin_perror ("chdir");
  }

  if (initgroups (username, groupid) == -1) {
    hin_error ("can't initgroups");
    goto finish;
  }

  if (setresgid (groupid, groupid, groupid) != 0) {
    hin_error ("can't set group id %d", groupid);
    goto finish;
  }

  if (setresuid (userid, userid, userid) != 0) {
    hin_error ("can't set user id %d", userid);
    goto finish;
  }

  if (setuid (0) == 0 || setgid (0) == 0) {
    hin_error ("didn't drop priv");
    goto finish;
  }

  ret = 0;
  if (hin_g.debug & HNDBG_CONFIG) {
    hin_debug ("switched user %s(%d):%s(%d)\n", username, userid, groupname, groupid);
  }

finish:
  if (pwd) free (pwd);
  if (grp) free (grp);
  if (pwd_buf) free (pwd_buf);
  if (grp_buf) free (grp_buf);
  return ret;
}

