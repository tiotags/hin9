
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/resource.h>

#include "hin.h"
#include "conf.h"

#include "hin_app.h"
#include "hin_child.h"

int hin_redirect_log (const char * path) {
  int fd = hin_open_file_and_create_path (AT_FDCWD, path, O_WRONLY | O_APPEND | O_CLOEXEC | O_CREAT | O_TRUNC, 0660);
  if (fd < 0) {
    hin_error ("can't open log '%s': %s\n", path, strerror (errno));
    exit (1);
  }

  fflush (stdout);
  fflush (stderr);
  setvbuf (stdout, NULL, _IOLBF, 1024);
  setvbuf (stderr, NULL, _IOLBF, 1024);

  if (dup2 (fd, STDOUT_FILENO) < 0) hin_perror ("dup2 stdout");
  if (dup2 (fd, STDERR_FILENO) < 0) hin_perror ("dup2 stderr");
  close (fd);

  if (hin_g.debug & HNDBG_CONFIG)
    hin_debug ("create log on %d '%s'\n", fd, path);

  return 0;
}

int hin_linux_set_limits () {
  struct rlimit new;
  rlim_t max;

  memset (&new, 0, sizeof (new));
  if (getrlimit (RLIMIT_MEMLOCK, &new) < 0) {
    hin_perror ("getrlimit");
  }

  max = new.rlim_max;
  max = max > HIN_RLIMIT_MEMLOCK ? HIN_RLIMIT_MEMLOCK : max;
  if (hin_g.debug & HNDBG_INFO)
    hin_debug ("rlimit MEMLOCK %lld/%lld/%lld\n", (long long)new.rlim_cur, (long long)max, (long long)new.rlim_max);

  new.rlim_cur = max;
  if (setrlimit (RLIMIT_MEMLOCK, &new) < 0) {
    hin_perror ("setrlimit");
  }

  if (new.rlim_cur < HIN_RLIMIT_MEMLOCK && (hin_g.debug & HNDBG_CONFIG)) {
    hin_debug ("WARNING! low RLIMIT_MEMLOCK, possible crashes, message possibly outdated\n");
    hin_debug (" current: %lld\n", (long long)new.rlim_cur);
    hin_debug (" suggested: %lld\n", (long long)HIN_RLIMIT_MEMLOCK);
  }

  memset (&new, 0, sizeof (new));
  if (getrlimit (RLIMIT_NOFILE, &new) < 0) {
    hin_perror ("getrlimit");
  }

  max = new.rlim_max;
  if (HIN_RLIMIT_NOFILE > max) {
    hin_debug ("rlimit requested nofile higher than max %lld < %lld\n", (long long)max, (long long)HIN_RLIMIT_NOFILE);
  } else {
    max = HIN_RLIMIT_NOFILE;
  }
  if (hin_g.debug & HNDBG_INFO)
    hin_debug ("rlimit NOFILE %lld/%lld/%lld\n", (long long)new.rlim_cur, (long long)max, (long long)new.rlim_max);

  new.rlim_cur = max;
  if (setrlimit (RLIMIT_NOFILE, &new) < 0) {
    hin_perror ("setrlimit");
  }

  return 0;
}

char * hin_directory_path (const char * old, const char ** replace) {
  if (replace && *replace) {
    free ((void*)*replace);
  }
  int len = strlen (old);
  char * new = NULL;
  if (old[len-1] == '/') {
    new = strdup (old);
    goto done;
  }
  new = malloc (len + 2);
  memcpy (new, old, len);
  new[len] = '/';
  new[len+1] = '\0';
done:
  if (replace) {
    *replace = new;
  }
  return new;
}

int hin_open_file_and_create_path (int dirfd, const char * path, int flags, mode_t mode) {
  int fd = openat (dirfd, path, flags, mode);
  if (fd >= 0) return fd;
  if (errno != ENOENT) return fd;
  if ((hin_g.flags & HIN_CREATE_DIRECTORY) == 0) return fd;

  mode_t mask = 0;
  if (mode & 0600) mask |= 0100;
  if (mode & 060) mask |= 010;
  if (mode & 06) mask |= 01;

  const char * ptr = path;
  while (1) {
    if (*ptr == '/') {
      ptr++;
      continue;
    }
    ptr = strchr (ptr, '/');
    if (ptr == NULL) return openat (dirfd, path, flags, mode);
    char * dir_path = strndup (path, ptr-path);
    int err = mkdirat (dirfd, dir_path, mode | mask);
    if (err == 0) {
      if (hin_g.debug & HNDBG_CONFIG) {
        hin_debug ("created for '%s' directory '%s'\n", path, dir_path);
      }
      free (dir_path);
      continue;
    }
    free (dir_path);

    switch (errno) {
    case ENOENT: break;
    case EEXIST: break;
    default:
      return -1;
    break;
    }
  }
}


