
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <basic_args.h>

#include "hin.h"
#include "conf.h"
#include "http/vhost.h"

#include "hin_app.h"

typedef struct {
  const char * nshort;
  const char * nlong;
  const char * help;
  int cmd;
  int opt;
  int only_main;
} hin_arg_t;

enum {CVER=1, CHELP, CPIDFILE, CDAEMONIZE, CPRETEND,
CDIR,
CWORKDIR, CLOGDIR, CTMPDIR, CCONFIG, CDLOG, CREUSE,
CVERBOSE, CQUIET, CLOGLEVEL, CLOGMASK,
CDOWNLOAD, COUTPUT, CPROGRESS, CAUTONAME, CLISTEN, CVHOST,
CEPOLL, CUSER};

static hin_arg_t cmdlist[] = {
{"-v", "--version", "print version info", CVER, 0, 0},
{"-h", "--help", "print help", CHELP, 0, 0},
{"-d", "--download", "download file and exit", CDOWNLOAD, 0, 0},
{"-o", "--output", "save download to file", COUTPUT, 0, 0},
{"-p", "--progress", "show download progress", CPROGRESS, 0, 0},
{"-n", "--autoname", "derive download name from url", CAUTONAME, 0, 0},
{NULL, "--serve", NULL, CLISTEN, 0, 0},
{NULL, "--listen", "start server on <port> without loading any config file", CLISTEN, 0, 0},
{"-c", "--config", "load config file at path", CCONFIG, 0, 0},
{NULL, "--log", "set debug log file path", CDLOG, 0, 0},
{NULL, "--pretend", NULL, CPRETEND, 0, 0},
{NULL, "--check", "check config file and exit", CPRETEND, 0, 0},
{NULL, "--pidfile", "set output pidfile", CPIDFILE, 0, 0},
{NULL, "--daemonize", "daemonize service", CDAEMONIZE, 0, 0},
{NULL, "--workdir", "set work dir path", CWORKDIR, CWORKDIR, 0},
{NULL, "--logdir", "set log dir path", CLOGDIR, CLOGDIR, 0},
{NULL, "--tmpdir", "set tmp dir path", CTMPDIR, CTMPDIR, 0},
{NULL, "--reuse", "internal, don't use", CREUSE, 0, 0},
{NULL, "--epoll", "force epoll backend", CEPOLL, 0, 0},
{NULL, "--user", "drop root privileges and switch to user", CUSER, 0, 0},
{"-V", "--verbose", "verbose output", CVERBOSE, 0, 0},
{"-q", "--quiet", "print only errors", CQUIET, 0, 0},
{NULL, "--loglevel", "0 prints only errors, 5 prints everything", CLOGLEVEL, 0, 0},
{NULL, "--logmask", "debugmask in hex", CLOGMASK, 0, 0},
{NULL, NULL, NULL, 0, 0, 0},
};

static http_client_t * current_download = NULL;

static void print_help () {
  printf ("usage hinsightd [OPTION]...\n");
  for (int i=0; (size_t)i < sizeof (cmdlist) / sizeof (cmdlist[0]) - 1; i++) {
    hin_arg_t * cmd = &cmdlist[i];
    if (cmd->nshort) {
      printf (" %s", cmd->nshort);
    } else {
      printf ("   ");
    }
    printf (" %s", cmd->nlong);
    if (cmd->help) {
      printf ("\t%s", cmd->help);
    }
    printf ("\n");
  }
}

int hin_process_argv (basic_args_t * args, const char * name) {
  hin_arg_t * cmd = NULL;

  for (int i=0; (size_t)i < sizeof (cmdlist) / sizeof (cmdlist[0]); i++) {
    cmd = &cmdlist[i];
    if ((cmd->nshort && strcmp (name, cmd->nshort) == 0) ||
        (cmd->nlong && strcmp (name, cmd->nlong) == 0)) {
      break;
    }
  }

  switch (cmd->cmd) {
  case CVER:
    printf ("%s", HIN_HTTPD_SERVER_BANNER);
    #ifdef HIN_USE_OPENSSL
    printf (" openssl");
    #endif
    #ifdef HIN_USE_RPROXY
    printf (" rproxy");
    #endif
    #ifdef HIN_USE_FCGI
    printf (" fcgi");
    #endif
    #ifdef HIN_USE_CGI
    printf (" cgi");
    #endif
    #ifdef HIN_USE_FFCALL
    printf (" ffcall");
    #endif
    printf ("\n");
    return 1;
  break;
  case CHELP:
    print_help ();
    return 1;
  break;
  case CDAEMONIZE:
    hin_g.flags |= HIN_DAEMONIZE;
  break;
  case CPRETEND:
    hin_g.flags |= HIN_PRETEND;
    httpd_vhost_set_debug (0);
  break;
  case CWORKDIR:
  case CLOGDIR:
  case CTMPDIR: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("missing %s path", cmd->nlong);
      print_help ();
      return -1;
    }
    switch (cmd->cmd) {
    case CWORKDIR:
      hin_directory_path (path, &hin_app.workdir_path);
      if (chdir (hin_app.workdir_path) < 0) hin_perror ("chdir");
    break;
    case CLOGDIR:
      hin_directory_path (path, &hin_app.logdir_path);
    break;
    case CTMPDIR:
      hin_directory_path (path, &hin_app.tmpdir_path);
    break;
    }
  break; }
  case CPIDFILE: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("missing %s path", cmd->nlong);
      print_help ();
      return -1;
    }
    if (*path) hin_app.pid_path = path;
  break; }
  case CCONFIG: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("missing %s path", cmd->nlong);
      print_help ();
      return -1;
    }
    hin_app.conf_path = path;
    hin_g.flags &= ~HIN_SKIP_CONFIG;
  break; }
  case CDLOG: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("missing %s path", cmd->nlong);
      print_help ();
      return -1;
    }
    if (hin_redirect_log (path) < 0) return -1;
  break; }
  case CREUSE: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("don't use --reuse");
      print_help ();
      return -1;
    }
    int fd = atoi (path);
    if (fcntl (fd, F_GETFD) == -1) {
      hin_error ("fd %d is not opened", fd);
      exit (1);
    }
    hin_app.restart_pipe_fd = fd;
  break; }
  case CVERBOSE:
    httpd_vhost_set_debug (0xffffffff);
  break;
  case CQUIET:
    httpd_vhost_set_debug (0x0);
  break;
  case CLOGLEVEL: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("missing argument for %s", cmd->nlong);
      print_help ();
      return -1;
    }
    int nr = atoi (path);
    hin_g.debug = 0;
    switch (nr) {
    case 5: hin_g.debug = 0xffffffff; break;
    case 4: // fall through
    case 3: // fall through
    case 2: hin_g.debug |= HNDBG_HTTP|HNDBG_CGI|HNDBG_PROXY; // fall through
    case 1: hin_g.debug |= HNDBG_CONFIG|HNDBG_SOCKET|HNDBG_RW_ERROR; // fall through
    case 0: hin_g.debug |= HNDBG_BASIC; break;
    default: hin_error ("unkown loglevel '%s'", path); return -1; break;
    }
    httpd_vhost_set_debug (hin_g.debug);
  break; }
  case CLOGMASK: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("missing argument for %s", cmd->nlong);
      print_help ();
      return -1;
    }
    httpd_vhost_set_debug (strtol (path, NULL, 16));
  break; }
  case CDOWNLOAD: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("missing argument for %s", cmd->nlong);
      print_help ();
      return -1;
    }
    http_client_t * http_download_raw (http_client_t * http, const char * url1);
    http_client_t * http = http_download_raw (NULL, path);
    current_download = http;
    if (hin_g.debug == HIN_DEBUG_MASK)
      hin_g.debug &= ~(HNDBG_BASIC | HNDBG_CONFIG);
    hin_g.flags |= HIN_SKIP_CONFIG;
    hin_stop ();
  break; }
  case COUTPUT: {
    const char * path = basic_args_get (args);
    if (path == NULL) {
      hin_error ("missing argument for %s", cmd->nlong);
      print_help ();
      return -1;
    }
    if (current_download == NULL) {
      hin_error ("no current download for %s", cmd->nlong);
      return -1;
    }
    http_client_t * http = current_download;
    http->save_fd = open (path, O_RDWR | O_CLOEXEC | O_TRUNC | O_CREAT, 0666);
    if (http->save_fd < 0) {
      hin_error ("can't open '%s': %s", path, strerror (errno));
      return -1;
    }
  break; }
  case CPROGRESS:
    if (current_download == NULL) {
      hin_error ("no current download for %s", cmd->nlong);
      return -1;
    }
    http_client_t * http = current_download;
    http->debug |= HNDBG_PROGRESS;
  break;
  case CAUTONAME: {
    if (current_download == NULL) {
      hin_error ("no current download for %s", cmd->nlong);
      return -1;
    }
    http_client_t * http = current_download;
    http->flags |= HIN_FLAG_AUTONAME;
  break; }
  case CLISTEN: {
    const char * htdocs = ".";
    const char * port = basic_args_get (args);
    if (port == NULL) {
      hin_error ("missing argument for %s", cmd->nlong);
      print_help ();
      return -1;
    }
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("serving folder '%s' on port %s\n", htdocs, port);
    httpd_server_t * sock = httpd_create (NULL, port, NULL, NULL);
    hin_bind_default_vhost (sock);
    hin_g.flags |= HIN_SKIP_CONFIG;
  break; }
  case CEPOLL: {
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("using epoll only\n");
    hin_g.flags |= HIN_FORCE_EPOLL;
  break; }

  case CUSER: {
    const char * username = basic_args_get (args);
    const char * groupname = basic_args_get (args);
    if (username == NULL) {
      hin_error ("missing username", cmd->nlong);
      return -1;
    }
    if (groupname == NULL) {
      groupname = username;
    }
    hin_app.username = strdup (username);
    hin_app.groupname = strdup (groupname);
  break; }

  default:
    hin_error ("unkown option '%s'", name);
    print_help ();
    return -1;
  break;
  }
  return 0;
}


