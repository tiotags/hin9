
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <basic_pattern.h>

#include "hin.h"
#include "conf.h"

#include "hin_app.h"
#include "hin_lua.h"

static hin_buffer_t * console_buffer = NULL;

int console_execute (string_t * source) {
  if (matchi_string_equal (source, "q\n") > 0) {
    printf ("do quit\n");
    hin_app_stop ();
  } else if (matchi_string_equal (source, "restart\n") > 0) {
    hin_app_restart ();
  } else if (matchi_string_equal (source, "reload\n") > 0) {
    int lua_reload ();
    if (lua_reload () < 0)
      hin_error ("reload failed");
  } else if (matchi_string (source, "lua ") > 0) {
    hin_lua_run (source->ptr, source->len);
  } else {
    hin_lua_run (source->ptr, source->len);
  }
  return 0;
}

static int hin_console_read_callback (hin_buffer_t * buf, int ret) {
  if (ret <= 0) {
    if (ret < 0) {
      hin_error ("console: %s", strerror (-ret));
    }
    if (hin_g.debug & HNDBG_CONFIG) hin_debug ("console EOF\n");
    if (isatty (0)) {
      // if is a tty then stop the server
      hin_app_stop ();
    }
    return 0;
  }
  string_t temp;
  temp.ptr = buf->ptr;
  temp.len = ret;
  buf->ptr[ret] = '\0';
  console_execute (&temp);
  if (hin_request_read (buf) < 0) {
    hin_weird_error (897455);
    return -1;
  }
  return 0;
}

void hin_console_clean () {
  if (console_buffer) {
    console_buffer->flags &= ~HIN_ACTIVE;
    hin_buffer_stop_clean (console_buffer);
  }
}

int hin_timer_cb (int ms) {
  int hin_timeout_callback (float dt);
  hin_timeout_callback (1);
  int hin_timer_check ();
  hin_timer_check ();
  return 0;
}

int hin_console_init () {
  hin_timer_init (hin_timer_cb);

  if (!isatty (0)) {
    hin_debug ("console not connected to a terminal\n");
  }

  hin_buffer_t * buf = malloc (sizeof (*buf) + READ_SZ);
  memset (buf, 0, sizeof (*buf));
  #ifdef HIN_LINUX_BUG_5_11_3
  buf->flags |= HIN_EPOLL;
  #endif
  buf->fd = STDIN_FILENO;
  buf->callback = hin_console_read_callback;
  buf->count = buf->sz = READ_SZ;
  buf->ptr = buf->buffer;
  buf->debug = hin_g.debug;
  if (hin_request_read (buf) < 0) {
    hin_error ("console init failed");
    hin_buffer_clean (buf);
    return -1;
  }
  console_buffer = buf;
  return 0;
}



