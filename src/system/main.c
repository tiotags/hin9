
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <basic_args.h>
#include <basic_pattern.h>
#include <basic_vfs.h>

#include "hin.h"
#include "conf.h"

#include "hin_app.h"

hin_app_t hin_app;

void hin_app_clean () {
  hin_stop ();
  hin_log_flush ();
  hin_lua_clean ();
  hin_console_clean ();
  hin_cache_clean ();
  hin_signal_clean ();
  hin_vfs_clean ();
  httpd_vhost_clean ();
  #ifdef HIN_USE_FCGI
  hin_fcgi_clean ();
  #endif
  // shouldn't clean pidfile it can incur a race condition
  free ((void*)hin_app.exe_path);
  free ((void*)hin_app.logdir_path);
  free ((void*)hin_app.workdir_path);
  free ((void*)hin_app.tmpdir_path);
  free ((void*)hin_app.username);
  free ((void*)hin_app.groupname);

  //close (0); close (1); close (2);

  hin_clean ();

  if (hin_g.debug & HNDBG_BASIC)
    hin_debug ("hin close ...\n");
  #ifdef BASIC_USE_MALLOC_DEBUG
  hin_debug ("num fds open %d\n", print_fds ());
  print_unfree ();
  #endif
}

void hin_app_stop () {
  hin_timer_flush ();
  http_connection_close_idle ();
  httpd_connection_close_idle ();
  #ifdef HIN_USE_FCGI
  hin_fcgi_clean ();
  #endif
  hin_stop ();
}

int main (int argc, const char * argv[], const char * envp[]) {
  memset (&hin_g, 0, sizeof hin_g);
  memset (&hin_app, 0, sizeof hin_app);
  hin_app.conf_path = HIN_CONF_PATH;
  hin_app.exe_path = realpath ((char*)argv[0], NULL);
  hin_app.argv = argv;
  hin_app.envp = envp;
  hin_g.debug = HIN_DEBUG_MASK;
  hin_g.max_client = HIN_HTTPD_DEFAULT_MAX_CONNECTIONS;
  hin_g.cork_size = READ_SZ;
  hin_directory_path (HIN_LOGDIR_PATH, &hin_app.logdir_path);
  hin_directory_path (HIN_WORKDIR_PATH, &hin_app.workdir_path);
  hin_directory_path (HIN_TEMP_PATH, &hin_app.tmpdir_path);

  int hin_process_argv (basic_args_t * args, const char * name);
  int ret = basic_args_process (argc, argv, hin_process_argv);
  if (ret) {
    if (ret > 0) return 0;
    return -1;
  }

  if (hin_g.debug & HNDBG_BASIC)
    hin_debug ("hin start ...\n");

  if (HIN_RESTRICT_ROOT && geteuid () == 0 && hin_app.username == NULL) {
    if (HIN_RESTRICT_ROOT == 1) {
      hin_debug ("WARNING! process started as root\n");
    } else if (HIN_RESTRICT_ROOT == 2) {
      hin_error ("not allowed to run as root");
      exit (1);
    }
  }

  int hin_linux_set_limits ();
  hin_linux_set_limits ();

  hin_init ();

  hin_console_init ();
  hin_signal_init ();

  int lua_init ();
  void hin_lua_report_error ();
  if (lua_init () < 0) {
    hin_lua_report_error ();
    hin_error ("could not init lua");
    return -1;
  }

  int hin_conf_load (const char * path);
  if (hin_conf_load (hin_app.conf_path) < 0) {
    hin_lua_report_error ();
    return -1;
  }

  if (hin_g.flags & HIN_PRETEND) {
    return 0;
  }
  if (hin_g.flags & HIN_DAEMONIZE) {
    int hin_daemonize ();
    if (hin_daemonize () < 0) { return -1; }
  }
  // pid file always goes after daemonize
  if (hin_app.pid_path) {
    int hin_pidfile (const char * path);
    if (hin_pidfile (hin_app.pid_path) < 0) { return -1; }
  }

  if ((hin_g.flags & HIN_FLAG_RUN) == 0) {
    hin_error ("no work to do, exiting");
    return -1;
  } else if (hin_g.debug & HNDBG_BASIC) {
    hin_debug ("hin serve ...\n");
  }

  if (hin_app.username) {
    if (hin_drop_user (hin_app.username, hin_app.groupname) < 0)
      return -1;
  }

  hin_app_restart_finished ();

  hin_event_loop ();

  hin_app_clean ();

  return 0;
}


