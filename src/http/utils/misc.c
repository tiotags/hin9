
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <sys/socket.h>

#include "hin.h"
#include "http/http.h"

HIN_EXPORT int httpd_file_is_multirange (httpd_client_t * http) {
  if (http->status != 206) return 0;
  if (http->range_requests.next == NULL) return 0;
  if (http->range_requests.next->next == NULL) return 0;
  return 1;
}

int httpd_timeout_callback (hin_timer_t * timer, time_t tm) {
  httpd_client_t * http = (httpd_client_t*)timer->ptr;
  int do_close = 0;
  if (http->state & (HIN_REQ_HEADERS | HIN_REQ_POST | HIN_REQ_END)) do_close = 1;
  if (http->debug & HNDBG_TIMEOUT)
    hin_debug ("httpd %d timer shutdown %lld state %x %s\n", http->c.sockfd, (long long)tm, http->state, do_close ? "close" : "wait");
  if (do_close == 0) {
    hin_timer_update (timer, time (NULL) + 5);
    return 0;
  }
  hin_timer_remove (timer);
  shutdown (http->c.sockfd, SHUT_RD);
  httpd_client_shutdown (http);
  return 1;
}

void httpd_client_ping (httpd_client_t * http, int timeout) {
  hin_timer_t * timer = &http->timer;
  time_t tm = time (NULL) + timeout;
  hin_timer_update (timer, tm);
  if (http->debug & HNDBG_TIMEOUT)
    hin_debug ("httpd %d timeout %p at %lld\n", http->c.sockfd, timer->ptr, (long long)timer->time);
}

time_t hin_date_str_to_time (string_t * source) {
  struct tm tm;
  time_t t;
  if (strptime (source->ptr, "%a, %d %b %Y %X GMT", &tm) == NULL) {
    hin_error ("can't strptime '%.*s'", 20, source->ptr);
    return 0;
  }
  tm.tm_isdst = -1; // Not set by strptime(); tells mktime() to determine whether daylight saving time is in effect
  t = mktime (&tm);
  if (t == -1) {
    hin_error ("can't mktime");
    return 0;
  }
  return t;
}

HIN_EXPORT int httpd_error (httpd_client_t * http, int status, const char * fmt, ...) {
  va_list ap, ap1;
  va_start (ap, fmt);
  va_copy (ap1, ap);

  fprintf (stderr, "error! httpd %d %d ", http->c.sockfd, status);
  vfprintf (stderr, fmt, ap);
  fprintf (stderr, "\n");

  if (status == 0) {
    va_end (ap);
    return 0;
  }

  char * body = NULL;
  if (hin_g.flags & HIN_VERBOSE_ERRORS) {
    char * msg = NULL;
    if (vasprintf ((char**)&msg, fmt, ap1) < 0) {
      hin_perror ("asprintf");
    }
    if (asprintf ((char**)&body, "<html><head></head><body><h1>Error %d: %s</h1><p>%s</p></body></html>\n", status, http_status_name (status), msg) < 0)
      hin_perror ("asprintf");
    free (msg);
  }
  httpd_respond_fatal (http, status, body);
  if (body) free (body);

  va_end (ap);
  return 0;
}

int httpd_request_chunked (httpd_client_t * http);

int hin_client_deflate_init (httpd_client_t * http) {
  http->z.zalloc = Z_NULL;
  http->z.zfree = Z_NULL;
  http->z.opaque = Z_NULL;
  int windowsBits = 15;
  if (http->peer_flags & HIN_HTTP_DEFLATE) {
    http->peer_flags = (http->peer_flags & ~HIN_HTTP_COMPRESS) | HIN_HTTP_DEFLATE;
  } else if (http->peer_flags & HIN_HTTP_GZIP) {
    http->peer_flags = (http->peer_flags & ~HIN_HTTP_COMPRESS) | HIN_HTTP_GZIP;
    windowsBits |= 16;
  } else {
    hin_error ("useless zlib init");
    return -1;
  }
  int ret = deflateInit2 (&http->z, Z_DEFAULT_COMPRESSION, Z_DEFLATED, windowsBits, 8, Z_DEFAULT_STRATEGY);
  if (ret != Z_OK) {
    hin_error ("deflate init failed");
    return -1;
  }
  httpd_request_chunked (http);
  return 0;
}

