
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"

#include <basic_endianness.h>

#include "fcgi.h"

FCGI_Header * hin_fcgi_header (hin_buffer_t * buf, int type, int id, int sz) {
  FCGI_Header * head = hin_header_ptr (buf, sizeof (*head));
  head->version = FCGI_VERSION_1;
  head->type = type;
  head->request_id = endian_swap16 (id);
  head->length = endian_swap16 (sz);
  head->padding = 0;

  if (buf->debug & HNDBG_CGI)
    hin_debug ("fcgi %d worker %d type %d sz %d packet\n", buf->fd, id, type, sz);

  return head;
}



