
#if defined (HIN_USE_CGI) || defined (HIN_USE_FCGI)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"

#include "fcgi.h"

int hin_cgi_location (httpd_client_t * http, hin_buffer_t * buf, string_t * origin) {
  string_t source = *origin;
  //if (http->status == 200) http->status = 302; // bad idea ?
  if (match_string (&source, "http") > 0) {
    hin_header (buf, "Location: %.*s\r\n", origin->len, origin->ptr);
    return 0;
  }
  if (hin_g.debug & HNDBG_CGI)
    hin_error ("cgi %d location2 %.*s", http->c.sockfd, (int)origin->len, origin->ptr);
  hin_header (buf, "Location: %.*s\r\n", origin->len, origin->ptr);
  return 0;
}

void hin_cgi_worker_free (hin_fcgi_worker_t * worker) {
  #ifdef HIN_USE_FCGI
  hin_fcgi_socket_t * socket = worker->socket;
  if (socket) {
    socket->worker[worker->req_id] = NULL;

    socket->num_worker--;

    if (socket->num_worker <= 0)
      hin_fcgi_socket_close (socket);
  }
  #endif
  free (worker);
}

#endif

