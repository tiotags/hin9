
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <basic_pattern.h>

#include "hin.h"
#include "http/http.h"
#include "http/file.h"
#include "http/vhost.h"
#include "conf.h"

#include "httpd_internal.h"

HIN_EXPORT int httpd_write_common_headers (httpd_client_t * http, hin_buffer_t * buf) {
  if ((http->disable & HIN_HTTP_DATE) == 0) {
    time_t rawtime;
    time (&rawtime);
    hin_header_date (buf, "Date: " HIN_HTTP_DATE_FORMAT "\r\n", rawtime);
  }
  if ((http->disable & HIN_HTTP_BANNER) == 0) {
    hin_header (buf, "Server: %s\r\n", HIN_HTTPD_SERVER_BANNER);
  }
  if (http->peer_flags & HIN_HTTP_DEFLATE) {
    hin_header (buf, "Content-Encoding: deflate\r\n");
  } else if (http->peer_flags & HIN_HTTP_GZIP) {
    hin_header (buf, "Content-Encoding: gzip\r\n");
  }
  if (http->peer_flags & HIN_HTTP_CHUNKED) {
    hin_header (buf, "Transfer-Encoding: chunked\r\n");
  }
  if ((http->disable & HIN_HTTP_CACHE) == 0 && http->cache_flags) {
    header_cache_control (buf, http->cache_flags, http->cache);
  }
  if (http->peer_flags & HIN_HTTP_KEEPALIVE) {
    hin_header (buf, "Connection: keep-alive\r\n");
  } else {
    hin_header (buf, "Connection: close\r\n");
  }

  if (httpd_file_is_multirange (http)) {
    // if multipart print content-type in the fragment header
  } else if (http->content_type) {
    hin_header (buf, "Content-Type: %s\r\n", http->content_type);
  }
  httpd_vhost_t * vhost = http->vhost;
  if (vhost && vhost->hsts && (vhost->vhost_flags & HIN_HSTS_NO_HEADER) == 0) {
    hin_header (buf, "Strict-Transport-Security: max-age=%d", vhost->hsts);
    if (vhost->vhost_flags & HIN_HSTS_SUBDOMAINS)
      hin_header (buf, "; includeSubDomains");
    if (vhost->vhost_flags & HIN_HSTS_PRELOAD)
      hin_header (buf, "; preload");
    hin_header (buf, "\r\n");
  }
  if (http->append_headers) {
    hin_header (buf, "%s", http->append_headers);
  }
  return 0;
}

static int http_raw_response_callback (hin_buffer_t * buf, int ret) {
  httpd_client_t * http = (httpd_client_t*)buf->parent;

  if (ret < 0) {
    hin_error ("httpd %d sending error %s\n", buf->fd, strerror (-ret));
    httpd_client_shutdown (http);
    return -1;
  } else if (hin_buffer_continue_write (buf, ret) > 0) {
    http->count += ret;
    return 0;
  }

  http->count += ret;

  http->state &= ~HIN_REQ_ERROR;
  httpd_client_finish_request (http);

  return 1;
}

HIN_EXPORT int httpd_respond_text (httpd_client_t * http, int status, const char * body) {
  if (http->state & HIN_REQ_DATA) return -1;
  http->state |= HIN_REQ_DATA;

  if (http->method & HIN_METHOD_POST) {
    http->method = HIN_METHOD_GET;
    httpd_error (http, 405, "POST on a raw resource");
    return 0;
  }
  http->status = status;

  hin_buffer_t * buf = malloc (sizeof (*buf) + READ_SZ);
  memset (buf, 0, sizeof (*buf));
  buf->flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->fd = http->c.sockfd;
  buf->callback = http_raw_response_callback;
  buf->count = 0;
  buf->sz = READ_SZ;
  buf->ptr = buf->buffer;
  buf->parent = http;
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;

  int freeable = 0;
  if (body == NULL) {
    freeable = 1;
    if (asprintf ((char**)&body, "<html><head></head><body><h1>Error %d: %s</h1></body></html>\n", status, http_status_name (status)) < 0)
      hin_perror ("asprintf");
  }
  http->disable |= HIN_HTTP_CHUNKED | HIN_HTTP_COMPRESS | HIN_HTTP_CACHE;
  http->peer_flags &= ~ http->disable;

  hin_header (buf, "HTTP/1.%d %d %s\r\n", http->peer_flags & HIN_HTTP_VER10 ? 0 : 1, status, http_status_name (status));

  httpd_write_common_headers (http, buf);
  hin_header (buf, "Content-Length: %ld\r\n", strlen (body));
  hin_header (buf, "\r\n");
  if (http->method != HIN_METHOD_HEAD) {
    hin_header (buf, "%s", body);
  }
  if (freeable) free ((char*)body);

  if (http->debug & HNDBG_RW)
    hin_debug ("httpd %d raw response %d '\n%.*s'\n", http->c.sockfd, buf->count, buf->count, buf->ptr);
  hin_request_write (buf);

  return 0;
}

HIN_EXPORT int httpd_respond_error (httpd_client_t * http, int status, const char * body) {
  if (http->state & HIN_REQ_ERROR) return 0;
  http->state &= ~(HIN_REQ_DATA | HIN_REQ_POST);
  http->state |= HIN_REQ_ERROR;
  http->method = HIN_METHOD_GET;
  return httpd_respond_text (http, status, body);
}

HIN_EXPORT int httpd_respond_fatal (httpd_client_t * http, int status, const char * body) {
  if (http->state & HIN_REQ_ERROR) return 0;
  http->state &= ~(HIN_REQ_DATA | HIN_REQ_POST);
  http->state |= HIN_REQ_ERROR;
  http->method = HIN_METHOD_GET;
  http->peer_flags &= ~HIN_HTTP_KEEPALIVE;
  httpd_respond_text (http, status, body);
  return 0;
}

HIN_EXPORT int httpd_respond_fatal_and_full (httpd_client_t * http, int status, const char * body) {
  if (http->state & HIN_REQ_ERROR) return 0;
  http->state &= ~(HIN_REQ_DATA | HIN_REQ_POST);
  http->state |= HIN_REQ_ERROR;
  http->method = HIN_METHOD_GET;
  http->peer_flags &= ~HIN_HTTP_KEEPALIVE;
  httpd_respond_text (http, status, body);
  return 0;
}

HIN_EXPORT int httpd_respond_buffer (httpd_client_t * http, int status, hin_buffer_t * data) {
  if (http->state & HIN_REQ_DATA) return -1;
  http->state |= HIN_REQ_DATA;
  http->status = status;

  hin_buffer_t * buf = malloc (sizeof (*buf) + READ_SZ);
  memset (buf, 0, sizeof (*buf));
  buf->flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->fd = http->c.sockfd;
  buf->callback = http_raw_response_callback;
  buf->count = 0;
  buf->sz = READ_SZ;
  buf->ptr = buf->buffer;
  buf->parent = http;
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;

  off_t len = 0;
  for (basic_dlist_t * elem = &data->list; elem; elem = elem->next) {
    hin_buffer_t * buf = hin_buffer_list_ptr (elem);
    len += buf->count;
  }

  http->disable |= HIN_HTTP_CHUNKED | HIN_HTTP_COMPRESS | HIN_HTTP_CACHE;
  http->peer_flags &= ~ http->disable;
  hin_header (buf, "HTTP/1.%d %d %s\r\n", http->peer_flags & HIN_HTTP_VER10 ? 0 : 1, status, http_status_name (status));
  httpd_write_common_headers (http, buf);
  hin_header (buf, "Content-Length: %ld\r\n", len);
  hin_header (buf, "\r\n");
  if (http->debug & HNDBG_RW)
    hin_debug ("httpd %d buffer response %d '\n%.*s'\n", http->c.sockfd, buf->count, buf->count, buf->ptr);

  if (http->method != HIN_METHOD_HEAD) {
    basic_dlist_t * elem = &buf->list;
    while (elem->next) elem = elem->next;
    basic_dlist_add_after (NULL, elem, &data->list);
  } else {
    basic_dlist_t * elem = &data->list;
    while (elem) {
      hin_buffer_t * buf1 = hin_buffer_list_ptr (elem);
      elem = elem->next;
      hin_buffer_clean (buf1);
    }
  }

  hin_request_write (buf);
  return 0;
}

HIN_EXPORT int httpd_respond_redirect (httpd_client_t * http, int status, const char * location) {
  char * new = NULL;
  char * old = http->append_headers;
  int num = asprintf (&new, "Location: %s\r\n%s", location, old ? old : "");
  if (num < 0) { if (new) free (new); return -1; }

  if (old) free (old);
  http->append_headers = new;
  httpd_respond_text (http, status ? status : 302, "");
  return 0;
}

HIN_EXPORT int httpd_respond_redirect_https (httpd_client_t * http) {
  char * new = NULL;
  int num = asprintf (&new, "https://%s%s", http->hostname, http->path);
  if (num < 0) { if (new) free (new); return -1; }

  httpd_respond_redirect (http, 302, new);
  free (new);
  return 0;
}



