
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <basic_vfs.h>

#include "hin.h"
#include "http/http.h"
#include "http/vhost.h"

#include "httpd_internal.h"

int hin_app_is_overloaded () {
  if (hin_request_is_overloaded ()) return 1;
  if (hin_g.max_client && hin_g.num_client > hin_g.max_client) {
    //printf ("overloaded %d/%d\n", hin_g.num_client, hin_g.max_client);
    return 1;
  }
  return 0;
}

static int httpd_client_buffer_close_callback (hin_buffer_t * buffer, int ret) {
  httpd_client_t * http = (httpd_client_t*)buffer->parent;
  if (http->debug & HNDBG_HTTP)
    hin_debug ("httpd %d shutdown buffer %s\n", http->c.sockfd, ret < 0 ? strerror (-ret) : "");
  httpd_client_shutdown (http);
  return 0;
}

static int httpd_client_buffer_eat_callback (hin_buffer_t * buffer, int num) {
  if (num > 0) {
  } else if (num == 0) {
    hin_lines_request (buffer, 0);
  }
  return 0;
}

static int httpd_client_accept (hin_client_t * client) {
  httpd_client_t * http = (httpd_client_t*)client;
  httpd_server_t * server = http->c.parent;
  http->debug = server->s.debug;

  httpd_client_start_request (http);

  hin_buffer_t * buf = hin_lines_create_raw (READ_SZ);
  buf->fd = http->c.sockfd;
  buf->parent = http;
  buf->flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;
  http->read_buffer = buf;

  if (hin_app_is_overloaded ()) {
    httpd_error (http, 503, "overload");
    return 0;
  }

  hin_timer_t * timer = &http->timer;
  int httpd_timeout_callback (hin_timer_t * timer, time_t time);
  timer->callback = httpd_timeout_callback;
  timer->ptr = http;

  hin_lines_t * lines = (hin_lines_t*)&buf->buffer;
  lines->read_callback = server->read_callback;
  lines->close_callback = httpd_client_buffer_close_callback;
  lines->eat_callback = httpd_client_buffer_eat_callback;
  hin_lines_request (buf, 0);
  return 0;
}

void * httpd_client_sni_callback (hin_client_t * client, const char * name, int len) {
  httpd_client_t * http = (httpd_client_t*)client;
  httpd_server_t * server = (httpd_server_t*)http->c.parent;
  httpd_vhost_t * parent_vhost = (httpd_vhost_t*)server->s.c.parent;
  httpd_vhost_t * vhost = httpd_vhost_get (parent_vhost, name, len);
  if (vhost == NULL || vhost->ssl_ctx == NULL) {
    if (http->debug & (HNDBG_SSL|HNDBG_INFO))
      hin_debug ("ssl can't find vhost '%s'\n", name);
    return NULL;
  }
  return vhost->ssl_ctx;
}

HIN_EXPORT httpd_annoyance_t default_annoy = {
.small_read_size = 300,
.max_annoyance = 500,
};

int httpd_increase_annoyance (httpd_client_t * http, int annoyance) {
  http->annoyance += annoyance;
  httpd_annoyance_t * annoy = &default_annoy;

  if (http->annoyance > annoy->max_annoyance) {
    httpd_respond_fatal (http, 429, NULL);
    if (http->debug & (HNDBG_ANNOY|HNDBG_RW_ERROR))
      hin_debug ("http %d '%s' annoy %d %d/%d drop\n", http->c.sockfd, http->path, annoyance, http->annoyance, annoy->max_annoyance);
    return -1;
  } else {
    if (http->debug & HNDBG_ANNOY)
      hin_debug ("http %d '%s' annoy %d %d/%d\n", http->c.sockfd, http->path, annoyance, http->annoyance, annoy->max_annoyance);
  }
  return 0;
}

HIN_EXPORT int hin_httpd_start (httpd_server_t * server, const char * addr, const char * port, const char * sock_type, void * ssl_ctx) {
  server->s.accept_callback = httpd_client_accept;
  server->s.sni_callback = httpd_client_sni_callback;
  server->s.ssl_ctx = ssl_ctx;
  server->s.accept_flags |= SOCK_CLOEXEC;
  server->s.debug |= hin_g.debug;
  server->s.flags |= HIN_SERVER_HTTPD;

  if (server->s.user_data_size == 0)
    server->s.user_data_size = sizeof (httpd_client_t);

  if (server->read_callback == NULL)
    server->read_callback = httpd_client_read_callback;

  if (hin_g.debug & (HNDBG_BASIC|HNDBG_SOCKET))
    hin_debug ("http%sd listening on '%s':'%s'\n", ssl_ctx ? "s" : "", addr ? addr : "all", port);

  int err = hin_request_listen (&server->s, addr, port, sock_type);
  if (err < 0) {
    hin_error ("listen socket");
    // free socket
    return -1;
  }

  return 1;
}

