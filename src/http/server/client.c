
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "conf.h"
#include "http/http.h"
#include "http/cache.h"
#include "http/vhost.h"

#include "httpd_internal.h"

#include <basic_vfs.h>

int httpd_client_reread (httpd_client_t * http);

HIN_EXPORT void httpd_client_clean (httpd_client_t * http) {
  if (http->debug & (HNDBG_HTTP|HNDBG_MEMORY)) hin_debug ("httpd %d clean\n", http->c.sockfd);
  if (http->file_path) free ((void*)http->file_path);
  if (http->post_sep) free ((void*)http->post_sep);

  if (http->file_fd) {
    if (http->debug & (HNDBG_HTTP|HNDBG_SYSCALL)) hin_debug ("  close file_fd %d\n", http->file_fd);
    close (http->file_fd);
  }
  if (http->post_fd) {
    if (http->debug & (HNDBG_HTTP|HNDBG_SYSCALL)) hin_debug ("  close post_fd %d\n", http->post_fd);
    close (http->post_fd);
  }
  http->file_fd = http->post_fd = 0;

  if (http->append_headers) free (http->append_headers);
  if (http->content_type) free (http->content_type);
  if (http->hostname) free (http->hostname);
  if (http->path) free (http->path);
  if (http->query) free (http->query);

  int ret = deflateEnd (&http->z);
  if (ret != Z_OK) {
  }

  http->peer_flags = http->disable = 0;
  http->status = http->method = 0;
  http->count = 0;
  http->cache = http->modified_since = 0;
  http->cache_flags = 0;
  http->etag = http->post_sz = 0;
  http->post_sep = http->file_path = http->append_headers = NULL;
  http->hostname = http->content_type = NULL;
  http->path = http->query = NULL;
  http->file = NULL;
  http->headers.len = 0;
  http->headers.ptr = NULL;
  http->vhost = NULL;
  http->annoyance = 0;

  basic_dlist_t * elem = http->range_requests.next;
  while (elem) {
    httpd_client_range_t * range = basic_dlist_ptr (elem, offsetof (httpd_client_range_t, list));
    elem = elem->next;

    basic_dlist_remove (&http->range_requests, &range->list);
    free (range);
  }
  http->current_range = NULL;

  hin_timer_remove (&http->timer);
}

HIN_EXPORT int httpd_client_start_request (httpd_client_t * http) {
  http->state = HIN_REQ_HEADERS | (http->state & HIN_REQ_STOPPING);

  if (http->debug & HNDBG_HTTP) {
    hin_debug ("http%sd %d request begin %lld\n",
      (http->c.flags & HIN_SSL) ? "s" : "", http->c.sockfd, (long long)time (NULL));
  }
  http->status = 200;

  httpd_server_t * server = (httpd_server_t*)http->c.parent;
  httpd_vhost_t * vhost = (httpd_vhost_t*)server->s.c.parent;
  httpd_vhost_switch (http, vhost);

  if (server->begin_callback)
    server->begin_callback (http);

  return 0;
}

HIN_EXPORT int httpd_client_finish_request (httpd_client_t * http) {
  http->state &= ~(HIN_REQ_DATA | HIN_REQ_POST);

  int keep = (http->peer_flags & HIN_HTTP_KEEPALIVE) && ((http->state & HIN_REQ_STOPPING) == 0);
  if (http->debug & HNDBG_HTTP) hin_debug ("httpd %d request done %s\n", http->c.sockfd, keep ? "keep" : "close");

  httpd_server_t * server = http->c.parent;
  if (server->finish_callback)
    server->finish_callback (http);

  hin_lines_eat (http->read_buffer, http->headers.len);

  if (keep) {
    httpd_client_clean (http);
    httpd_client_start_request (http);
    httpd_client_reread (http);
  } else {
    http->state |= HIN_REQ_END;
    httpd_client_shutdown (http);
  }

  return 0;
}

HIN_EXPORT int httpd_client_finish_output (httpd_client_t * http, hin_pipe_t * pipe) {
  http->count = pipe->out.count;
  http->state &= ~HIN_REQ_DATA;

  httpd_server_t * server = http->c.parent;
  if (server->finish_output_callback)
    server->finish_output_callback (http, pipe);

  if (http->state & (HIN_REQ_DATA|HIN_REQ_POST)) return 0;
  return httpd_client_finish_request (http);
}

static int httpd_client_close_callback (hin_buffer_t * buffer, int ret) {
  httpd_client_t * http = (httpd_client_t*)buffer->parent;
  if (ret < 0) {
    hin_error ("httpd %d client close callback: %s", http->c.sockfd, ret < 0 ? strerror (-ret) : "");
    return -1;
  }
  if (http->debug & (HNDBG_HTTP)) hin_debug ("httpd %d close\n", http->c.sockfd);

  hin_buffer_stop_clean (http->read_buffer);
  http->read_buffer = NULL;

  httpd_client_clean (http);
  hin_client_close (&http->c);
  return 1;
}

HIN_EXPORT int httpd_client_shutdown (httpd_client_t * http) {
  if (http->state & HIN_REQ_STOPPING) return -1;
  http->state |= HIN_REQ_STOPPING;
  if (http->debug & (HNDBG_HTTP)) hin_debug ("httpd %d shutdown\n", http->c.sockfd);

  hin_buffer_t * buf = malloc (sizeof *buf);
  memset (buf, 0, sizeof (*buf));
  buf->flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->fd = http->c.sockfd;
  buf->callback = httpd_client_close_callback;
  buf->parent = (hin_client_t*)http;
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;

  if (hin_request_close (buf) < 0) {
    buf->flags |= HIN_SYNC;
    hin_request_close (buf);
  }
  return 0;
}


