
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <basic_pattern.h>

#include "hin.h"
#include "http/http.h"
#include "http/vhost.h"
#include "conf.h"

#include "httpd_internal.h"

time_t hin_date_str_to_time (string_t * source);

int httpd_vhost_request (httpd_client_t * http, const char * name, int len);

int httpd_parse_headers_line (httpd_client_t * http, string_t * line) {
  string_t param, param1, param2;

  if (matchi_string (line, "Accept-Encoding: ") > 0) {
    while (match_string (line, "([%w]+)", &param) > 0) {
      if (matchi_string (&param, "deflate") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
          hin_debug ("  can use deflate\n");
        http->peer_flags |= HIN_HTTP_DEFLATE;
      } else if (matchi_string (&param, "gzip") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
          hin_debug ("  can use gzip\n");
        http->peer_flags |= HIN_HTTP_GZIP;
      }
      if (match_string (line, "%s*,%s*") <= 0) break;
    }
  } else if (matchi_string (line, "Host:%s*([%w.]+)", &param1) > 0) {
    if (httpd_vhost_request (http, param1.ptr, param1.len) < 0) {
      // can't find hostname
    }
  } else if (matchi_string (line, "If-Modified-Since:%s*") > 0) {
    time_t tm = hin_date_str_to_time (line);
    http->modified_since = tm;
  } else if (matchi_string (line, "If-None-Match:%s*[\"]?") > 0) {
    uint64_t etag = strtol (line->ptr, NULL, 16);
    http->etag = etag;
  } else if (matchi_string (line, "Connection:%s*") > 0) {
    while (1) {
      if (matchi_string (line, "close") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
          hin_debug ("  close\n");
        http->peer_flags &= ~HIN_HTTP_KEEPALIVE;
      } else if (matchi_string (line, "keep-alive") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
          hin_debug ("  keepalive\n");
        http->peer_flags |= HIN_HTTP_KEEPALIVE;
      } else if (match_string (line, "%w+") > 0) {
      }
      if (match_string (line, "%s*,%s*") < 0) break;
    }
  } else if (matchi_string (line, "Range:%s*") > 0) {
    if (matchi_string (line, "bytes%s*=%s*") <= 0) {
      return 1;
    }
    param2.len = 0;
    while ((match_string (line, "(%d+)-(%d*)", &param1, &param2) > 0)
    || (match_string (line, "(-%d+)", &param1) > 0)) {
      httpd_client_range_t * range = calloc (1, sizeof *range);

      if (param1.len > 0) {
        range->start = atoi (param1.ptr);
      } else {
        range->start = 0;
      }
      if (param2.len > 0) {
        range->end = atoi (param2.ptr) + 1;
      } else {
        range->end = -1;
      }
      basic_dlist_append (&http->range_requests, &range->list);

      if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
        hin_debug ("  range requested is %lld-%lld\n", (long long)range->start, (long long)range->end);

      if (matchi_string (line, "%s*,%s*") <= 0) break;
    }
  } else if (http->method & HIN_METHOD_POST) {
    if (matchi_string (line, "Content-Length: (%d+)", &param) > 0) {
      http->post_sz = atoi (param.ptr);
      if (http->debug & (HNDBG_HTTP|HNDBG_POST))
        hin_debug ("  post length is %lld\n", (long long)http->post_sz);
      http->peer_flags |= HIN_HTTP_POST;
    } else if (matchi_string (line, "Content-Type:%s*multipart/form-data;%s*boundary=%\"?([%-%w]+)%\"?", &param) > 0) {
      char * new = malloc (param.len + 2 + 1);
      new[0] = '-';
      new[1] = '-';
      memcpy (&new[2], param.ptr, param.len);
      new[param.len + 2] = '\0';
      http->post_sep = new;
      if (http->debug & (HNDBG_HTTP|HNDBG_POST))
        hin_debug ("  post content type multipart/form-data boundry is '%s'\n", new);
    } else if (matchi_string (line, "Transfer-Encoding:%s*") > 0) {
      if (matchi_string (line, "chunked") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_POST))
          hin_debug ("  post content encoding is chunked\n");
        http->peer_flags |= (HIN_HTTP_CHUNKED_UPLOAD | HIN_HTTP_POST);
      } else if (matchi_string (line, "identity") > 0) {
      } else {
        httpd_error (http, 501, "doesn't accept post with transfer encoding");
        return 0;
      }
    }
  }
  return 1;
}

int httpd_parse_headers (httpd_client_t * http, string_t * source) {
  string_t line;
  string_t orig = *source;

  while (1) {
    if (source->len <= 0) return 0;
    if (hin_find_line (source, &line) == 0) return 0;
    if (line.len == 0) break;
  }

  *source = orig;

  if (hin_httpd_parse_path (http, source) < 0) {
    int status = 400;
    if (hin_http_version (http->peer_flags) == 0) { status = 505; }
    if (http->method == 0) { status = 501; }
    string_t line;
    hin_find_line (source, &line);
    httpd_error (http, status, "parsing request line '%.*s'", (int)line.len, line.ptr);
    if (http->debug & (HNDBG_RW|HNDBG_RW_ERROR))
      hin_debug (" raw request '\n%.*s'\n", (int)orig.len, orig.ptr);
    return -1;
  }

  int version = hin_http_version (http->peer_flags);
  if (http->debug & (HNDBG_HTTP|HNDBG_RW))
    hin_debug ("httpd %d method %x path '%s' query '%s' ver %x\n", http->c.sockfd,
      http->method, http->path, http->query, version);

  http->count = -1;
  if (version != 0x10) http->peer_flags |= HIN_HTTP_KEEPALIVE;

  while (hin_find_line (source, &line)) {
    if (http->debug & HNDBG_RW)
      hin_debug (" %d '%.*s'\n", (int)line.len, (int)line.len, line.ptr);
    if (line.len == 0) break;
    int ret = httpd_parse_headers_line (http, &line);
    if (ret <= 0) {
      *source = orig;
      if (ret < 0) {
        httpd_error (http, 400, "shouldn't happen");
      }
      return -1;
    }
  }

  if (http->peer_flags & HIN_HTTP_CHUNKED_UPLOAD) {
    http->post_sz = 0;
  }
  if (http->post_sz < 0) {
    http->post_sz = 0;
  }
  if (HIN_HTTPD_ERROR_MISSING_HOSTNAME && (http->peer_flags & HIN_HTTP_VER10) == 0 && http->hostname == NULL) {
    httpd_error (http, 400, "missing hostname");
    return -1;
  }
  if (http->peer_flags & http->disable & HIN_HTTP_POST) {
    httpd_error (http, 403, "post disabled");
    return -1;
  } if (http->method == HIN_METHOD_POST && (http->peer_flags & HIN_HTTP_POST) == 0) {
    httpd_error (http, 411, "post missing size");
    return -1;
  } else if (HIN_HTTPD_MAX_POST_SIZE && http->post_sz >= HIN_HTTPD_MAX_POST_SIZE) {
    httpd_error (http, 413, "post size %lld >= %ld", (long long)http->post_sz, (long)HIN_HTTPD_MAX_POST_SIZE);
    return -1;
  }

  return (uintptr_t)source->ptr - (uintptr_t)orig.ptr;
}

int httpd_parse_req (httpd_client_t * http, string_t * source) {
  int used = httpd_parse_headers (http, source);
  if (used <= 0) return used;

  http->peer_flags &= ~http->disable;
  http->state &= ~HIN_REQ_HEADERS;

  return used;
}


