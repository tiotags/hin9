
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hin.h"
#include "http/http.h"
#include "http/vhost.h"

#include "hin_lua.h"

static int httpd_vhost_map_add_to_end_list (httpd_vhost_map_t ** list, httpd_vhost_map_t * new) {
  new->next = new->prev = NULL;
  httpd_vhost_map_t * last = *list;
  while (last && last->next) {
    last = last->next;
  }
  if (last == NULL) {
    *list = new;
  } else {
    new->prev = last;
    (last)->next = new;
  }
  return 0;
}

static int httpd_vhost_map_free (httpd_vhost_map_t * map) {
  if (map->pattern) free ((void*)map->pattern);
  free (map);
  return 0;
}

void httpd_vhost_map_clean (httpd_vhost_t * vhost) {
  httpd_vhost_map_t * next;
  for (int i=0; i<HIN_VHOST_MAP_LAST; i++) {
    for (httpd_vhost_map_t * map = vhost->maps[i]; map; map = next) {
      next = map->next;
      httpd_vhost_map_free (map);
    }
    vhost->maps[i] = NULL;
  }
}

static int hin_vhost_pattern_match (const char * name, const char * pattern) {
  const char * ptr = name;
  while (1) {
    if (*pattern == '*') return 1;
    if (*ptr == '\0') {
      if (*pattern == '\0') return 1;
      return 0;
    }
    if (*pattern == '\0') return 0;
    if (*ptr != *pattern) return 0;
    pattern++;
    ptr++;
  }
  return 0;
}

static int hin_vhost_run_map (httpd_client_t * http, httpd_vhost_t * vhost, httpd_vhost_map_t * map) {
  lua_State * L = vhost->L;
  lua_rawgeti (L, LUA_REGISTRYINDEX, map->callback);
  lua_pushlightuserdata (L, http);

  if (lua_pcall (L, 1, 1, 0) != 0) {
    hin_error ("map callback '%s' '%s'", vhost->hostname, lua_tostring (L, -1));
    lua_pop (L, 1);
    return -1;
  }

  int a = 0;
  if (lua_isboolean (L, -1)) {
    a = lua_toboolean (L, -1);
  }
  lua_pop (L, 1);

  return a;
}

int httpd_vhost_map_callback (httpd_client_t * http, int type) {
  httpd_vhost_t * vhost = http->vhost;

  while (vhost) {
    httpd_vhost_map_t * map_start = vhost->maps[type];

    for (httpd_vhost_map_t * map = map_start; map; map = map->next) {
      if (hin_vhost_pattern_match (http->path, map->pattern) == 0)
        continue;

      if (hin_vhost_run_map (http, vhost, map) != 0) {
        goto finalize;
      }
    }

    if (vhost->parent == NULL && vhost != httpd_vhost_default ()) {
      vhost = httpd_vhost_default ();
    } else {
      vhost = vhost->parent;
    }
  }
finalize:
  return 1;
}

int l_hin_map (lua_State *L) {
  httpd_vhost_t * vhost = (httpd_vhost_t*)lua_touserdata (L, 1);
  if (vhost == NULL) {
    vhost = httpd_vhost_default ();
  }
  if (vhost == NULL || vhost->magic != HIN_VHOST_MAGIC) {
    return luaL_error (L, "invalid vhost");
  }
  const char * path = lua_tostring (L, 2);

  if (!lua_isfunction (L, 4)) {
    return luaL_error (L, "requires a callback");
  }
  lua_pushvalue (L, 4);
  int callback = luaL_ref (L, LUA_REGISTRYINDEX);

  httpd_vhost_map_t * map = calloc (1, sizeof (*map));
  map->pattern = strdup (path);
  map->state = lua_tonumber (L, 3);
  map->vhost = vhost;
  map->lua = L;
  map->callback = callback;

  if (hin_g.debug & HNDBG_CONFIG)
    hin_debug ("add map %d for %s:%s\n", map->state, vhost->hostname, path);

  if (map->state >= HIN_VHOST_MAP_LAST || map->state < 0) {
    httpd_vhost_map_free (map);
    luaL_error (L, "requires a valid state");
    return 0;
  }

  httpd_vhost_map_add_to_end_list (&vhost->maps[map->state], map);

  return 0;
}

