
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>

#include <basic_vfs.h>

#include "hin.h"
#include "conf.h"
#include "http/http.h"
#include "http/vhost.h"

#include "hin_app.h"
#include "hin_lua.h"

static lua_State * internal_lua = NULL;

int hin_timeout_callback (float dt) {
  lua_State * L = internal_lua;

  lua_getglobal (L, "timeout_callback");
  if (lua_isnil (L, 1)) { return 0; }
  lua_pushnumber (L, 1);

  if (lua_pcall (L, 1, 0, 0) != 0) {
    hin_error ("timeout callback '%s'", lua_tostring (L, -1));
    lua_pop (L, 1);
    return -1;
  }

  return 0;
}

void lua_server_clean (httpd_vhost_t * server) {
  void httpd_vhost_map_clean (httpd_vhost_t * vhost);
  httpd_vhost_map_clean (server);
}

void hin_lua_clean () {
  basic_dlist_t * elem = hin_g.vhost_list.next;
  while (elem) {
    httpd_vhost_t * vhost = basic_dlist_ptr (elem, offsetof (httpd_vhost_t, list));
    elem = elem->next;

    lua_server_clean (vhost);
  }

  elem = hin_app.cert_list.next;
  while (elem) {
    hin_ssl_ctx_t * box = basic_dlist_ptr (elem, offsetof (hin_ssl_ctx_t, list));
    elem = elem->next;

    hin_ssl_ctx_unref (box);
  }

  lua_close (internal_lua);
  internal_lua = NULL;
}

void hin_lua_report_error () {
  lua_State *L = internal_lua;
  const char * reason = lua_tostring (L, -1);
  hin_error ("lua '%s'", reason);
}

int lua_init_constants (lua_State *L) {
  lua_newtable (L);

  lua_pushstring (L, "MAP_PREFILE");
  lua_pushnumber (L, HIN_VHOST_MAP_PREFILE);
  lua_settable (L, -3);

  lua_pushstring (L, "MAP_PROCESS");
  lua_pushnumber (L, HIN_VHOST_MAP_PROCESS);
  lua_settable (L, -3);

  lua_pushstring (L, "MAP_FINISH");
  lua_pushnumber (L, HIN_VHOST_MAP_FINISH);
  lua_settable (L, -3);

  lua_setglobal (L, "const");
  return 0;
}

int lua_init () {
  int err = 0;

  lua_State *L = luaL_newstate ();
  if (L == NULL) return -1;
  luaL_openlibs (L);

  int hin_lua_req_init (lua_State * L);
  err |= hin_lua_req_init (L);
  int hin_lua_opt_init (lua_State * L);
  err |= hin_lua_opt_init (L);
  int hin_lua_config_init (lua_State * L);
  err |= hin_lua_config_init (L);
  int hin_lua_os_init (lua_State * L);
  err |= hin_lua_os_init (L);
  int hin_lua_utils_init (lua_State * L);
  err |= hin_lua_utils_init (L);

  err |= lua_init_constants (L);

  if (err < 0) return err;

  lua_pushstring (L, hin_app.logdir_path);
  lua_setglobal (L, "logdir");
  lua_pushstring (L, hin_app.workdir_path);
  lua_setglobal (L, "cwd");

  internal_lua = L;

  return 0;
}

int hin_conf_load (const char * path) {
  if (hin_g.flags & HIN_SKIP_CONFIG) return 0;

  if (hin_g.debug & HNDBG_CONFIG)
    hin_debug ("lua config '%s'\n", hin_app.conf_path);

  if (run_file (internal_lua, path)) {
    hin_error ("can't load config at '%s'", path);
    return -1;
  }
  return 0;
}

int hin_lua_run (const char * data, int len) {
  lua_State * L = internal_lua;
  int ret = hin_lua_run_string (L, data, len, "console");
  if (ret < 0) {
    hin_error ("lua parsing '%.*s': %s", len, data, lua_tostring (L, -1));
    lua_pop (L, lua_gettop (L));
  }
  return ret;
}

int lua_reload () {
  hin_lua_clean ();
  lua_init ();
  return 0;
}


