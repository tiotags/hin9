
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hin.h"
#include "conf.h"
#include "http/http.h"
#include "http/cache.h"
#include "http/vhost.h"

#include "hin_app.h"
#include "hin_lua.h"
#include "hin_child.h"

#include <fcntl.h>
#include <sys/socket.h>
#include <netdb.h>

#ifdef HIN_USE_FFCALL
#include <avcall.h>
#endif

hin_buffer_t * logs = NULL;

static int hin_log_write_callback (hin_buffer_t * buf, int ret) {
  if (ret < 0) {
    hin_error ("log write: %s", strerror (-ret));
    return -1;
  }
  if (ret < buf->count) {
    buf->ptr += ret;
    buf->count -= ret;
    if (buf->flags & HIN_OFFSETS)
      buf->pos += ret;
    if (hin_request_write (buf) < 0) {
      buf->flags |= HIN_SYNC;
      hin_request_write (buf);
    }
    return 0;
  }
  return 1;
}

static int hin_log_flush_single (hin_buffer_t * buf) {
  int ret = pwrite (buf->fd, buf->buffer, buf->count, buf->pos);
  if (ret < buf->count) { hin_perror ("log write: %d<%d", ret, buf->count); }
  return 0;
}

static const char * hin_log_skip_format (const char * init) {
  const char * ptr = init;
  ptr++;
  while (*ptr == '-' || *ptr == '+' || *ptr == ' ' || *ptr == '#' || *ptr == '0') ptr++;
  while ((*ptr >= '0' && *ptr <= '9') || *ptr == '.') ptr++;
  if (*ptr == 'l') { ptr++; }
  if (*ptr == '*') {
    return init; // force exit I don't think this one is useful atm
  }
  return ptr;
}

static int l_hin_log_callback (lua_State *L) {
  hin_buffer_t * buf = lua_touserdata (L, lua_upvalueindex (1));
  int print_date = lua_toboolean (L, lua_upvalueindex (2));
  httpd_client_t * http = NULL;
  int param = 1;

  if (lua_islightuserdata (L, param)) {
    http = (httpd_client_t*)lua_touserdata (L, param);
    if (http == NULL || http->c.magic != HIN_CLIENT_MAGIC) {
      return luaL_error (L, "requires valid client");
    }
    param++;
  }

  size_t len = 0;
  const char * fmt = lua_tolstring (L, param, &len);
  if (fmt == NULL) { return luaL_error (L, "fmt nil"); }

  if (print_date) {
    time_t t;
    time (&t);
    hin_header_date (buf, "%F %R ", t);
  }

#ifdef HIN_USE_FFCALL
  int ret = 0;
  av_alist alist;
  av_start_int (alist, hin_header, &ret);
  av_ptr (alist, void *, buf);
  av_ptr (alist, char *, fmt);
  int nparam = 2;
  char * format = strdup (fmt);
  for (const char * ptr = format; *ptr; ptr++) {
    if (*ptr != '%') continue;
    ptr = hin_log_skip_format (ptr);
    switch (*ptr) {
    case 's':
      av_ptr (alist, char *, lua_tostring (L, nparam));
    break;
    case 'f': // fall-through
    case 'F': // fall-through
    case 'e': // fall-through
    case 'E': // fall-through
    case 'g': // fall-through
    case 'a': // fall-through
    case 'A':
      av_double (alist, lua_tonumber (L, nparam));
    break;
    case 'n':
    case '%':
    break;
    case 'p':
      av_int (alist, lua_tonumber (L, nparam));
    break;

    // ip, date, method, path+query, version, status, size
    // other: path, query
    case 'Q':
      if (http == NULL) break;
      /* TODO parse query
      if (http->query)
        hin_header (buf, "?%s", http->query);*/
      av_ptr (alist, char *, http->path);
      *ptr = 's';
    break;
    case 'S':
      if (http == NULL) break;
      av_int (alist, http->status);
      *ptr = 'd';
    break;
    case 'M':
      if (http == NULL) break;
      av_ptr (alist, char *, hin_http_method_name (http->method));
      *ptr = 's';
    break;
    case 'D': {
      if (http == NULL) break;
      /* TODO
      time_t t;
      time (&t);
      hin_header_date (buf, "%d/%m/%Y %H:%M:%S %Z", t);
      */
    break; }
    // ip
    case 'I':
      if (http == NULL) break;
      char buffer[NI_MAXHOST + NI_MAXSERV + 2];
      hin_client_addr (buffer, sizeof buffer, &http->c.ai_addr, http->c.ai_addrlen);
      av_ptr (alist, char *, buffer);
      *ptr = 's';
    break;
    // version
    case 'V':
      if (http == NULL) break;
      av_int (alist, hin_http_version (http->peer_flags));
      *ptr = 'x';
    break;
    // size
    case 'Z':
      if (http == NULL) break;
      av_int (alist, http->count);
      *ptr = 'd';
    break;

    default:
      av_int (alist, lua_tonumber (L, nparam));
    break;
    }
    nparam++;
  }
  av_call (alist);
  free (format);
#else
  const char * max = fmt + len;
  const char * last = fmt;
  char newfmt[30];
  int ret = 2;
  for (const char * ptr = fmt; ptr < max; ptr++) {
    if (*ptr != '%') continue;

    int len = ptr - last;
    hin_header_raw (buf, last, len);

    last = ptr;
    ptr = hin_log_skip_format (ptr);
    len = ptr-last + 1;
    if (len >= (int)sizeof (newfmt)) continue;
    memcpy (newfmt, last, len);
    newfmt[len] = '\0';
    switch (*ptr) {
    case 's':
      hin_header (buf, newfmt, lua_tostring (L, ret++));
    break;
    case 'f': // fall-through
    case 'F': // fall-through
    case 'e': // fall-through
    case 'E': // fall-through
    case 'g': // fall-through
    case 'G': // fall-through
    case 'a': // fall-through
    case 'A':
      hin_header (buf, newfmt, lua_tonumber (L, ret++));
    break;
    case 'u': // fall-through
    case 'i': // fall-through
    case 'd': // fall-through
    case 'o': // fall-through
    case 'x': // fall-through
    case 'X': // fall-through
    case 'p': // fall-through
    case 'c':
      hin_header (buf, newfmt, lua_tointeger (L, ret++));
    break;
    // nothing
    case 'n': // fall-through
    // format specifiers
    case 'h': // fall-through
    case 'l': // fall-through
    case 'j': // fall-through
    case 'z': // fall-through
    case 't': // fall-through
    case 'L': // fall-through
    break;
    // ip, date, method, path+query, version, status, size
    // other: path, query
    case 'Q':
      if (http == NULL) break;
      hin_header (buf, "%s", http->path);
      if (http->query)
        hin_header (buf, "?%s", http->query);
    break;
    case 'S':
      if (http == NULL) break;
      hin_header (buf, "%d", http->status);
    break;
    case 'M':
      if (http == NULL) break;
      hin_header (buf, "%s", hin_http_method_name (http->method));
    break;
    case 'D': {
      if (http == NULL) break;
      time_t t;
      time (&t);
      hin_header_date (buf, "%d/%m/%Y %H:%M:%S %Z", t);
    break; }
    // ip
    case 'I':
      if (http == NULL) break;
      char buffer[NI_MAXHOST + NI_MAXSERV + 3];
      hin_client_addr (buffer, sizeof buffer, &http->c.ai_addr, http->c.ai_addrlen);
      hin_header (buf, "%s", buffer);
    break;
    // version
    case 'V':
      if (http == NULL) break;
      hin_header (buf, "HTTP/%x", hin_http_version (http->peer_flags));
    break;
    // size
    case 'Z':
      if (http == NULL) break;
      hin_header (buf, "%d", http->count);
    break;
    default: hin_header (buf, "%s", newfmt);
    }
    last = ptr+1;
  }
  hin_header_raw (buf, last, max-last);
#endif
  hin_buffer_t * next = hin_buffer_list_ptr (buf->list.next);
  if (next == NULL) { return 0; }

  lua_pushlightuserdata (L, next);
  lua_replace (L, lua_upvalueindex (1));

  logs = next;
  next->pos = buf->pos + buf->count;

  if (hin_request_write (buf) < 0) {
    buf->flags |= HIN_SYNC;
    hin_request_write (buf);
  }

  return 0;
}

int hin_log_flush () {
  if (logs) {
    hin_log_flush_single (logs);
    close (logs->fd);
    hin_buffer_clean (logs);
  }
  return 0;
}

static int l_hin_nil_log_callback (lua_State *L) {
  return 0;
}

static int l_hin_nil_log (lua_State *L) {
  if (hin_g.debug & HNDBG_CONFIG)
    hin_debug ("create nil log\n");

  lua_pushcclosure (L, l_hin_nil_log_callback, 0);
  return 1;
}

static int l_hin_create_log (lua_State *L) {
  const char * path = lua_tostring (L, 1);
  int print_date = lua_toboolean (L, 2);
  int force_alive = lua_toboolean (L, 3);
  if (path == NULL) {
    if (force_alive) return luaL_error (L, "create_log path nil");
    return l_hin_nil_log (L);
  }
  int fd = hin_open_file_and_create_path (AT_FDCWD, path, O_WRONLY | O_APPEND | O_CLOEXEC | O_CREAT, 0660);
  if (fd < 0) {
    return luaL_error (L, "create_log '%s' %s\n", path, strerror (errno));
  }

  if (hin_g.debug & HNDBG_CONFIG)
    hin_debug ("create log on %d '%s'\n", fd, path);

  int sz = READ_SZ;
  hin_buffer_t * buf = malloc (sizeof (hin_buffer_t) + sz);
  memset (buf, 0, sizeof (hin_buffer_t));
  buf->flags = HIN_FILE | HIN_OFFSETS;
  buf->fd = fd;
  buf->count = 0;
  buf->sz = sz;
  buf->pos = 0;
  buf->ptr = buf->buffer;
  buf->debug = hin_g.debug;
  buf->callback = hin_log_write_callback;

  lua_pushlightuserdata (L, buf);
  lua_pushboolean (L, print_date);
  lua_pushcclosure (L, l_hin_log_callback, 2);

  logs = buf;

  return 1;
}

int hin_lua_mask_from_str (lua_State * L, int pos, uint32_t * ptr1);

static int l_hin_redirect_log (lua_State *L) {
  int type;
  type = lua_type (L, 1);
  if (type == LUA_TSTRING) {
    const char * path = lua_tostring (L, 1);
    hin_redirect_log (path);
  } else if (type != LUA_TNONE && type != LUA_TNIL) {
    return luaL_error (L, "error! redirect log path needs to be a string\n");
  }

  if (hin_g.flags & HIN_PRETEND) return 0;

  if (!lua_isnil (L, 2)) {
    uint32_t mask = 0;
    hin_lua_mask_from_str (L, 2, &mask);
    httpd_vhost_set_debug (mask);
  }

  return 0;
}

static int l_hin_create_cert (lua_State *L) {
  const char * cert = lua_tostring (L, 1);
  const char * key = lua_tostring (L, 2);

  if (hin_g.debug & HNDBG_CONFIG) {
    char * new_cert = realpath (cert, NULL);
    char * new_key = realpath (key, NULL);
    hin_debug ("ssl cert %s:\t%s\n", cert, new_cert);
    hin_debug ("ssl key  %s:\t%s\n", key, new_key);
    free (new_cert);
    free (new_key);
  }

#ifdef HIN_USE_OPENSSL
  void * ctx = NULL;
  if (cert && key) {
    ctx = hin_ssl_create_cert (cert, key);
  }
#else
  if (cert || key) {
    hin_error ("ssl module not compiled");
  }
  void * ctx = NULL;
#endif
  if (ctx == NULL) {
    lua_pushboolean (L, 0);
    return 1;
  }
  hin_ssl_ctx_t * box = calloc (1, sizeof (*box));
  box->cert = strdup (cert);
  box->key = strdup (key);
  box->ctx = ctx;
  box->magic = HIN_CERT_MAGIC;
  box->refcount++;
  basic_dlist_append (&hin_app.cert_list, &box->list);
  lua_pushlightuserdata (L, box);
  return 1;
}

static int l_hin_create_fcgi (lua_State *L) {
  #ifdef HIN_USE_FCGI
  const char * uri = lua_tostring (L, 1);
  int min = lua_tointeger (L, 2);
  int max = lua_tointeger (L, 3);

  void * ptr = hin_fcgi_start (uri, min, max);

  if (ptr == NULL) {
    return luaL_error (L, "error! fcgi can't connect '%s'\n", uri);
  }

  lua_pushlightuserdata (L, ptr);
  return 1;
  #else
  return luaL_error (L, "error! fcgi not compiled\n");
  #endif
}

static int l_hin_create_cache (lua_State *L) {
  const char * path = lua_tostring (L, 1);
  off_t sz = lua_tonumber (L, 2);

  if (sz <= 0) sz = HIN_HTTPD_CACHE_MAX_SIZE;

  hin_cache_create (path, sz);

  return 0;
}

#define lua_struct_get(NAME) {\
  lua_pushstring (L, #NAME); \
  lua_gettable (L, 1); \
  if (lua_isnumber (L, -1)) { \
    annoy->NAME = lua_tonumber (L, -1); \
  } \
  lua_pop (L, 1); \
 }

static int l_hin_set_annoyance_values (lua_State *L) {
  if (lua_type (L, 1) != LUA_TTABLE) {
    return luaL_error (L, "requires a table");
  }
  extern httpd_annoyance_t default_annoy;
  httpd_annoyance_t * annoy = &default_annoy;

  lua_struct_get (small_read_size);
  lua_struct_get (max_annoyance);

  return 0;
}

int hin_lua_config_init (lua_State * L) {

static lua_function_t functs [] = {
  {"create_log",		l_hin_create_log },
  {"nil_log",			l_hin_nil_log },
  {"redirect_log",		l_hin_redirect_log },
  {"create_cert",		l_hin_create_cert },
  {"create_fcgi",		l_hin_create_fcgi },
  {"add_vhost",			l_hin_add_vhost },
  {"map",			l_hin_map },
  {"create_cache",		l_hin_create_cache },
  {"set_annoyance_values",	l_hin_set_annoyance_values },
  {NULL, NULL},
};

  return lua_add_functions (L, functs);
}

